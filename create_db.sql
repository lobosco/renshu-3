create table user (id int not null auto_increment, assoc_judoka_id int,pwd_hash blob, username text, primary key(id));
create table `groups` (id int not null auto_increment, group_name text, primary key(id));
create table groups_user (id int not null auto_increment, group_id int, user_id int, primary key(id));
create table permissions (id int not null auto_increment, group_id int, readable_judoka_id int, primary key(id));
create table slots (id int not null auto_increment, name text, begin text, end text, duration int, rate int, active int, primary key(id));
create table qualification (id int not null auto_increment, name text, rate int, primary key(id));
create table qualification_judoka (id int not null auto_increment, qualification_id int, judoka_id int, primary key(id));
create table judoka (id int not null auto_increment, name text, prename text, grade int, pass text, wkl text, contact text, birth_date date, dsgvo_photo int, examiner int, comment longtext, primary key(id));
create table training (id int not null auto_increment, date date, slot_id int, plan longtext, comment longtext, primary key(id));
create table training_trainer (id int not null auto_increment, training_id int, judoka_id_trainer int, primary key(id));
create table training_participants (id int not null auto_increment, training_id int, judoka_id_participant int, primary key(id));
create table competition (id int not null auto_increment, name text, date date, location text, primary key(id));
create table competition_coachs (id int not null auto_increment, competition_id int, judoka_id_coach int, primary key(id));
create table competition_participants (id int not null auto_increment, competition_id int, judoka_id_participant int, class text, result text, comment longtext, primary key(id));
create table exam (id int not null auto_increment, date date, begin text, end text, duration int, primary key(id));
create table exam_examiners (id int not null auto_increment, exam_id int, judoka_id_examiner int, primary key(id));
create table exam_participants (id int not null auto_increment, exam_id int, judoka_id_participant int, received_grade int, primary key(id));
create table request (id int not null auto_increment, user_id_issuer int, datetime_issued date, text longtext, done int, primary key(id));
create table sessions (id int, user_id int, count int, timer time, primary key(id));
create table salary_header (id int not null auto_increment, imagedata longblob not null, primary key(id));

insert into judoka (name,prename,grade,pass,wkl,contact,birth_date,dsgvo_photo,examiner,comment) values ("Müller","Franz","1","123456","123456MF","franz@mueller.de","1974-09-09","1","0","testuser");
insert into user (username,assoc_judoka_id,pwd_hash) values( "muellerfz","1", AES_ENCRYPT("12345678", UNHEX(SHA2("12345678",512))));
insert into `groups` (group_name) values ("admin");
insert into groups_user (group_id, user_id) values ("1","1");
insert into permissions (group_id, readable_judoka_id) values ("1","1");

-- Aufbau csv: Name,Vormane,Geburtsdatum,Graduierung,Passnummer,WKL,Kontakt,Prüfer,Photo,Lizenzen,Kommentar

