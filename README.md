lRenshu:
=======

Renshu Trainingsverwaltung Version 3.x

Features:
=========

- backend to securely connect to underlaying MySQL server
- encryption of any traffic via tls
- web frontend for accessing and administering databas

Version History: 
================

30.12.2018: Version 3.0


Prerequesites:
==============

- c compiler (for example gcc)
- boost (>= 1.70)
- mysql

Installation:
=============

Important: cloning the repository requires the --recursive flag, as it contains submodules.

For installation run the following commands: 

    cd backend
    make

This creates an executable in the backend subfolder, that can be run.

For logging purpose, the subfolder /renshu in /var/log needs to be created and be writable by the user that runs the backend.

    mkdir /var/log/renshu

To run the backend as a daemon, use the 'run_as_daemon.sh' bash script after correcting the paths.

For configuration and further assistance see documentation in the doc subfolder.

LICENSE:
========

Copyright (C) 2018 Wolfgang Knopki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
