var list_window;
var table_length = 0;
var salary_head;

function check_time_exam(begin,end,checked_begin){
    let beginarray = begin.split(":");
    let endarray = end.split(":");
    let checkedarray = checked_begin.split(":");
    
    if(beginarray[0]<checkedarray[0]){
        if(endarray[0]>checkedarray[0]){
            return true;
        }else if(endarray[0]==checkedarray[0]&endarray[1]>checkedarray[1]){
            return true;
        }else{
            return false;
        }
    }else if(beginarray[0]==checkedarray[0]&&beginarray[1]<=checkedarray[1]){
        return true;
    }else{
        return false;
    }
}


function set_training_template(window){
    window.name = "Trainingsliste";
    window.document.write("<head><style>th.rotate {width:30px;white-space: nowrap;}td.rotate {  width:30px;  white-space: nowrap;}th.rotate > div {  transform:    translate(0px,-7px)    rotate(270deg);  width: 30px;}td.rotate > div {  transform:     translate(0px,-7px)    rotate(270deg);  width: 30px;}td.date{width: 30px;textalign:center;border: 1px solid #ccc;}td.firstrow{width: 100px;border: 1px solid #ccc;}th.firstrow{width: 100px;}table{margin-top:100px;bordercollapse:collapse;font-size:10pt;}body{font-size:10pt;}</style> <meta charset='UTF-8'> </head>");
}

function set_salary_template(window){
    window.name = "Abrechnung";
    window.document.write("<head> <style>table{margin-left:80px;border-collapse:collapse;font-size:10pt;}table.salary{margin-left:00px;border-collapse:collapse;}th{text-align:left;} body{font-size:10pt;}</style> <meta charset='UTF-8'>  </head>");
}

function set_salary_body(window,info_salary){
    let month=document.getElementById("input_month").value;
    let datemonth=new Date(month);
    let locale="de-de";
    month = datemonth.toLocaleString(locale, {month:"long", year:"numeric"});
    window.document.write("<body> <img src='./header.png' alt='Smiley face' height='66' width='700'><table frame='box'> <tr><th>Name:</th><td>"+info_salary.name+" </td></tr><tr><th>Straße:</th><td>"+info_salary.street+"</td></tr><tr><th>PLZ, Ort:</th><td>"+info_salary.plz+"</td></tr></table><table frame='box'> <tr><th>Bank:</th><td>Institut:</td><td>"+info_salary.institute+"</td></tr><tr><th></th><td>IBAN:</td><td>"+info_salary.iban+"</td></tr><tr><th></th><td>BIC:</td><td>"+info_salary.bic+"</td></tr></table><h4>"+month+"</h4><table class='salary' border='1'><tr><th>Tag</th><th>Tätigkeit</th><th>Ausbildung</th><th colspan='2'>1. Training</th><th colspan='2'>2. Training</th><th colspan='2'>3. Training</th><th>Std.</th><th>Betrag in \u20AC</th></tr><tr><td></td><td></td><td></td><td>Start</td><td>Ende</td><td>Start</td><td>Ende</td><td>Start</td><td>Ende</td><td></td><td></td></tr>");
    for(let i=1;i<32;i++){
        window.document.write("<tr><td>"+i+"</td><td id='what"+i+"'></td><td id='lic"+i+"'></td><td id='begin"+i+"_1'></td><td id='end"+i+"_1'></td><td id='begin"+i+"_2'></td><td id='end"+i+"_2'></td><td id='begin"+i+"_3'></td><td id='end"+i+"_3'></td><td id='sum"+i+"'></td><td id='pay"+i+"'></td></tr>")
    }
    window.document.write("<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td id='sum_total'></td><td id='pay_total'></td></tr></table><p><b>Ich bestätige meine Angaben:</b></p><p><b>Gesehen Abteilungsleitung:</b></p></body>");
    
}

function parse_configfile_salary(path){
    let Reader = new FileReader();
    // files is a FileList of File objects. List some properties.
    Reader.readAsText(path);
    Reader.onload=function(e){
        let contents = e.target.result;
        let content_by_line = contents.split('\n');
        salary_head =  {name:content_by_line[0],street:content_by_line[1],bic:content_by_line[4],plz:content_by_line[2],institute:content_by_line[3],iban:content_by_line[5]};
        set_salary_body(list_window,salary_head);
    };
}

function print_salary(){
    let month=document.getElementById("input_month").value;
    let comp_checkbox = document.getElementById("input_print_comp");
    let exam_checkbox = document.getElementById("input_print_exam");
    list_window=window.open();
    list_window.blur();
    list_window.URL = list_window.URL || list_window.webkitURL;
    set_salary_template(list_window);
    let name = document.getElementById("input_salary_name").value;
    let info_salary;
    if(name){
        console.log("values entered");
        info_salary = {name:name,street:document.getElementById("input_salary_street").value,bic:document.getElementById("input_salary_bic").value,plz:document.getElementById("input_salary_plz").value,institute:document.getElementById("input_salary_institute").value,iban:document.getElementById("input_salary_iban").value};
        set_salary_body(list_window,info_salary);
    }else{
        console.log("no values entered");
        let configpath = document.getElementById("input_alternate_config").files[0];
        console.log(configpath);
        parse_configfile_salary(configpath);
    }
    let license_name = "none";
    let license_rate = 0.0;
    let durationsum=0;
    let request_qualification = {type:"get_salary_qualification",session:sessionid}
    post_server(request_qualification).then(function(response){
        let data=response;
        decodeURI(data);
        if(correct_query(data)){
            license_name = data[0]["name"];
            license_rate = data[0]["rate"];
            console.log(license_name);
            console.log(license_rate);
        }
    }).then(function(){
        let request_data = {type:"get_salary_data_trainings",session:sessionid,month:month};
        post_server(request_data).then(function(response){
            let durationcurrent = 0;
            let data=response;
            alert_no_login(data);
            decodeURI(data);
            if(correct_query(data)){
                for(let i=0; i<data.length;i++){
                    let part = data[i];
                    let date = new Date(part["date"]);
                    let day = date.getDate();
                    let whatdiv = list_window.document.getElementById("what"+day);
                    whatdiv.innerHTML+= part["name"] + " ";
                    let licdiv = list_window.document.getElementById("lic"+day);
                    licdiv.innerHTML = license_name;
                    let begindiv0 = list_window.document.getElementById("begin"+day+"_1");
                    let begindiv1 = list_window.document.getElementById("begin"+day+"_2");
                    let begindiv2 = list_window.document.getElementById("begin"+day+"_3");
                    let enddiv0 = list_window.document.getElementById("end"+day+"_1");
                    let enddiv1 = list_window.document.getElementById("end"+day+"_2");
                    let enddiv2 = list_window.document.getElementById("end"+day+"_3");
                    let durationdiv = list_window.document.getElementById("sum"+day);
                    let paydiv = list_window.document.getElementById("pay"+day);
                    durationsum = durationsum + part["duration"];
                    if(begindiv0.innerHTML == ""){
                            begindiv0.innerHTML = part["begin"];
                            enddiv0.innerHTML = part["end"];
                            durationdiv.innerHTML = parseFloat(part["duration"]/60).toFixed(2);
                            if(part["rate"] == "0"){
                                paydiv.innerHTML = parseFloat(parseFloat(durationdiv.innerHTML) * parseFloat(license_rate/100)).toFixed(2);
                            }else{
                                paydiv.innerHTML = parseFloat(part["rate"]/100).toFixed(2);
                            }
                    }else{
                        if(begindiv1.innerHTML == ""){
                            begindiv1.innerHTML = part["begin"];
                            enddiv1.innerHTML = part["end"];
                            durationcurrent = parseFloat(parseFloat(durationdiv.innerHTML) + parseFloat(part["duration"]/60)).toFixed(2);
                            durationdiv.innerHTML=durationcurrent;
                            if(part["rate"] == "0"){
                                paydiv.innerHTML = parseFloat(parseFloat(durationdiv.innerHTML) * parseFloat(license_rate/100)).toFixed(2);
                            }else{
                                paydiv.innerHTML = parseFloat(parseFloat(paydiv.innerHTML) + parseFloat(part["rate"]/100)).toFixed(2);
                            }
                        }else{
                            begindiv2.innerHTML = part["begin"];
                            enddiv2.innerHTML = part["end"];
                            durationcurrent = parseFloat(parseFloat(durationdiv.innerHTML) + parseFloat(part["duration"]/60)).toFixed(2);
                            durationdiv.innerHTML=durationcurrent;
                            if(part["rate"] == "0"){
                                paydiv.innerHTML = parseFloat(parseFloat(durationdiv.innerHTML) * parseFloat(license_rate/100)).toFixed(2);
                            }else{
                                paydiv.innerHTML = parseFloat(parseFloat(paydiv.innerHTML) + parseFloat(part["rate"]/100)).toFixed(2);
                            }
                        }
                    }
                }
            }
        }).then(function(){
            if(comp_checkbox.checked){
                let comp_slot_id=document.getElementById("input_print_comp_slot").value;
                let request_data = {type:"get_salary_data_competitions",session:sessionid,month:month,id:comp_slot_id};
                post_server(request_data).then(function(response){
                    let durationcurrent = 0;
                    let data=response;
                    alert_no_login(data);
                    decodeURI(data);
                    if(correct_query(data)){
                        for(let i=0; i<data.length;i++){
                            let part = data[i];
                            let date = new Date(part["date"]);
                            let day = date.getDate();
                            let whatdiv = list_window.document.getElementById("what"+day);
                            whatdiv.innerHTML+= decodeURIComponent(part["name"]) + " ";
                            let licdiv = list_window.document.getElementById("lic"+day);
                            licdiv.innerHTML = "Betreuung";
                            let begindiv0 = list_window.document.getElementById("begin"+day+"_1");
                            let begindiv1 = list_window.document.getElementById("begin"+day+"_2");
                            let begindiv2 = list_window.document.getElementById("begin"+day+"_3");
                            let enddiv0 = list_window.document.getElementById("end"+day+"_1");
                            let enddiv1 = list_window.document.getElementById("end"+day+"_2");
                            let enddiv2 = list_window.document.getElementById("end"+day+"_3");
                            let durationdiv = list_window.document.getElementById("sum"+day);
                            let paydiv = list_window.document.getElementById("pay"+day);
                            if(begindiv0.innerHTML == ""){
                                begindiv0.innerHTML = "-";
                                enddiv0.innerHTML = "-";
                                paydiv.innerHTML = parseFloat(part["rate"]/100).toFixed(2);
                            }else{
                                if(begindiv1.innerHTML == ""){
                                    begindiv1.innerHTML = "-";
                                    enddiv1.innerHTML = "-";
                                    paydiv.innerHTML = parseFloat(parseFloat(paydiv.innerHTML) + parseFloat(part["rate"]/100)).toFixed(2);
                                }else{
                                    begindiv2.innerHTML = "-";
                                    enddiv2.innerHTML = "-";
                                    paydiv.innerHTML = parseFloat(parseFloat(paydiv.innerHTML) + parseFloat(part["rate"]/100)).toFixed(2);
                                }
                            }
                        }
                    }
                }).then(function(){
                    if(exam_checkbox.checked===false){
                        let durationtotaldiv = list_window.document.getElementById("sum_total");
                        let durationtotal = 0;
                        let paytotaldiv = list_window.document.getElementById("pay_total");
                        let paytotal = 0;
                        for(let i=1;i<32;i++){
                            let durationdiv = list_window.document.getElementById("sum"+i);
                            if(durationdiv.innerHTML != ""){
                                durationtotal += parseFloat(durationdiv.innerHTML);
                            }
                            let paydiv=list_window.document.getElementById("pay"+i);
                            if(paydiv.innerHTML != ""){
                                paytotal += parseFloat(paydiv.innerHTML);
                            }
                        }
                        durationtotaldiv.innerHTML = parseFloat(durationtotal).toFixed(2);
                        paytotaldiv.innerHTML= parseFloat(paytotal).toFixed(2);
                        $('#dialog_print_salary').modal('toggle');
                    }
                });
            }
            if(exam_checkbox.checked){
                let exam_slot_id=document.getElementById("input_print_exam_slot").value;
                let request_data = {type:"get_salary_data_exams",session:sessionid,month:month,id:exam_slot_id};
                post_server(request_data).then(function(response){
                    let durationcurrent = 0;
                    let data=response;
                    alert_no_login(data);
                    decodeURI(data);
                    if(correct_query(data)){
                        for(let i=0; i<data.length;i++){
                            let part = data[i];
                            let date = new Date(part["date"]);
                            let day = date.getDate();
                            let begindiv0 = list_window.document.getElementById("begin"+day+"_1");
                            let begindiv1 = list_window.document.getElementById("begin"+day+"_2");
                            let begindiv2 = list_window.document.getElementById("begin"+day+"_3");
                            let enddiv0 = list_window.document.getElementById("end"+day+"_1");
                            let enddiv1 = list_window.document.getElementById("end"+day+"_2");
                            let enddiv2 = list_window.document.getElementById("end"+day+"_3");
                            let durationdiv = list_window.document.getElementById("sum"+day);
                            let paydiv = list_window.document.getElementById("pay"+day);
                            durationsum = durationsum + part["duration"];
                            if(begindiv0.innerHTML == ""){
                                let whatdiv = list_window.document.getElementById("what"+day);
                                whatdiv.innerHTML+= decodeURIComponent(part["name"]) + " ";
                                let licdiv = list_window.document.getElementById("lic"+day);
                                licdiv.innerHTML = "Prüfer";
                                begindiv0.innerHTML = part["begin"];
                                enddiv0.innerHTML = part["end"];
                                durationdiv.innerHTML = parseFloat(part["duration"]/60).toFixed(2);
                                console.log("duration pruefung 1\n" + durationdiv.innerHTML);
                                paydiv.innerHTML = parseFloat(Math.ceil(parseFloat(durationdiv.innerHTML)) * part["rate"]/100).toFixed(2);
                            }else{
                                if(begindiv1.innerHTML == ""){
                                    if(check_time_exam(begindiv0.innerHTML,enddiv0.innerHTML,part["begin"])===false){
                                        let whatdiv = list_window.document.getElementById("what"+day);
                                        whatdiv.innerHTML+= decodeURIComponent(part["name"]) + " ";
                                        let licdiv = list_window.document.getElementById("lic"+day);
                                        licdiv.innerHTML += " Prüfer";
                                        begindiv1.innerHTML = part["begin"];
                                        enddiv1.innerHTML = part["end"];
                                        durationcurrent = parseFloat(parseFloat(durationdiv.innerHTML) + parseFloat(part["duration"]/60)).toFixed(2);
                                        console.log("duration pruefung 2\n" + durationcurrent);
                                        durationdiv.innerHTML=durationcurrent;
                                        paydiv.innerHTML = parseFloat(parseFloat(paydiv.innerHTML) + Math.ceil(part["duration"]/60) * part["rate"]/100).toFixed(2);
                                    }
                                }else{
                                    if(check_time_exam(begindiv1.innerHTML,enddiv1.innerHTML,part["begin"])===false && check_time_exam(begindiv0.innerHTML,enddiv0.innerHTML,part["begin"])===false && begindiv2.innerHTML == ""){
                                        let whatdiv = list_window.document.getElementById("what"+day);
                                        whatdiv.innerHTML+= decodeURIComponent(part["name"]) + " ";
                                        let licdiv = list_window.document.getElementById("lic"+day);
                                        licdiv.innerHTML += " Prüfer";
                                        begindiv2.innerHTML = part["begin"];
                                        enddiv2.innerHTML = part["end"];
                                        durationcurrent = parseFloat(parseFloat(durationdiv.innerHTML) + parseFloat(part["duration"]/60)).toFixed(2);
                                        durationdiv.innerHTML=durationcurrent;
                                        paydiv.innerHTML = parseFloat(parseFloat(paydiv.innerHTML) + Math.ceil(part["duration"]/60) * part["rate"]/100).toFixed(2);
                                    }else if(begindiv2.innerHTML != ""){
                                        display_alert("Prüfung am " + part["date"] + " nicht eingetragen, zu viele Trainingsslots belegt");
                                    }
                                }
                            }
                        }
                    }
                }).then(function(){
                    let durationtotaldiv = list_window.document.getElementById("sum_total");
                    let durationtotal = 0;
                    let paytotaldiv = list_window.document.getElementById("pay_total");
                    let paytotal = 0;
                    for(let i=1;i<32;i++){
                        let durationdiv = list_window.document.getElementById("sum"+i);
                        if(durationdiv.innerHTML != ""){
                            console.log(durationdiv.innerHTML);
                            durationtotal += parseFloat(durationdiv.innerHTML);
                        }
                        let paydiv=list_window.document.getElementById("pay"+i);
                        if(paydiv.innerHTML != ""){
                            console.log(paydiv.innerHTML);
                            paytotal += parseFloat(paydiv.innerHTML);
                        }
                    }
                    durationtotaldiv.innerHTML = parseFloat(durationtotal).toFixed(2);
                    paytotaldiv.innerHTML= parseFloat(paytotal).toFixed(2);
                    $('#dialog_print_salary').modal('toggle');
                });
            }
        }).then(function(){
            if(comp_checkbox.checked === false && exam_checkbox.checked === false){
                let durationtotaldiv = list_window.document.getElementById("sum_total");
                let durationtotal = 0;
                let paytotaldiv = list_window.document.getElementById("pay_total");
                let paytotal = 0;
                for(let i=1;i<32;i++){
                    let durationdiv = list_window.document.getElementById("sum"+i);
                    if(durationdiv.innerHTML != ""){
                        console.log(durationdiv.innerHTML);
                        durationtotal += parseFloat(durationdiv.innerHTML);
                    }
                    let paydiv=list_window.document.getElementById("pay"+i);
                    if(paydiv.innerHTML != ""){
                        console.log(paydiv.innerHTML);
                        paytotal += parseFloat(paydiv.innerHTML);
                    }
                }
                durationtotaldiv.innerHTML = parseFloat(durationtotal).toFixed(2);
                paytotaldiv.innerHTML= parseFloat(paytotal).toFixed(2);
                $('#dialog_print_salary').modal('toggle');
            }
        });
    });
}

function print_comp(){
    let year = document.getElementById("input_wk_year").value;
    let request_data = {type:"get_competitors_list",session:sessionid,year:year};
    list_window = window.open();
    list_window.blur();
    list_window.name = "Wettkämpferliste";
    list_window.document.write("<head><style>body{font-size:10pt;}</style><meta charset='UTF-8'></head><body><h2> Wettkämpfer im Jahr "+year+"</h2><h3>U10</h3><div id='u10'></div><h3>U12</h3><div id='u12'></div><h3>U15</h3><div id='u15'></div><h3>U18</h3><div id='u18'></div><h3>U21</h3><div id='u21'></div><h3>Aktive</h3><div id='aktive'></div>");
    post_server(request_data).then(function(response){
        let data = response;
        alert_no_login(data);
        if(correct_query(data)){
            for(let i=0; i<data.length;i++){
                let part = data[i];
                let birth_date = new Date(part["birth_date"]);
                let activedate = new Date((year-21)+"-12-31");
                let u21date = new Date((year-18)+"-12-31");
                let u18date = new Date((year-15)+"-12-31");
                let u15date = new Date((year-12)+"-12-31");
                let u12date = new Date((year-10)+"-12-31");
                let u10date = new Date((year)+"-12-31");
                let printdiv=list_window.document.getElementById("aktive");
                if(birth_date<activedate){
                    console.log(birth_date, activedate);
                    printdiv=list_window.document.getElementById("aktive");
                }else if(birth_date<u21date){
                    console.log(birth_date, u21date);
                    printdiv=list_window.document.getElementById("u21");
                }else if(birth_date<u18date){
                    console.log(birth_date, u18date);
                    printdiv=list_window.document.getElementById("u18");
                }else if(birth_date<u15date){
                    console.log(birth_date, u15date);
                    printdiv=list_window.document.getElementById("u15");
                }else if(birth_date<u12date){
                    console.log(birth_date, u12date);
                    printdiv=list_window.document.getElementById("u12");
                }else if(birth_date<u10date){
                    console.log(birth_date, u10date);
                    printdiv=list_window.document.getElementById("u10");
                }
                let competitor_div = list_window.document.getElementById("participant"+part["participant_id"]);
                let competition_ul = list_window.document.getElementById("competitionlist"+ part["participant_id"]);
                if(competition_ul){
                    competition_ul.innerHTML+= "<li>"+ decodeURIComponent(part["compname"]) + " " + decodeURIComponent(part["location"]) + " " + decodeURIComponent(part["class"]) +" "+ part["result"] +"</li>"; 
                }else{
                    printdiv.innerHTML+="<div id='participant"+ part["participant_id"]+"'><p>" + decodeURIComponent(part["prename"]) +" "+ decodeURIComponent(part["name"]) +"</p><ul id='competitionlist"+ part["participant_id"]+"'><li>" + decodeURIComponent(part["compname"]) + " " + decodeURIComponent(part["location"]) + " " + decodeURIComponent(part["class"]) +" "+ part["result"] +"</li></ul></div>"; 
                }
            }
        }
        $('#dialog_print_comp').modal('toggle');
    });
}

function print_training(){
//     2) get list of pupils in period, each pupil carries a list of trainings
    list_window=window.open();
    list_window.blur();
    let training_slot = document.getElementById("input_list_slot").value;
    let training_from = document.getElementById("input_list_from").value;
    let training_to = document.getElementById("input_list_to").value;
    let id_all = [];
    table_length = 20;
    set_training_template(list_window);
    list_window.document.write("<body><h2>"+$("#input_list_slot").find('option:selected').text()+"</h2><table><tr id='header' class='firstrow'><th>Schüler</th><th class='rotate'><div><span>Geburtsdatum</span></div></th><th class='rotate'><div><span>Graduierung</span></div></th></tr></thead><tbody id='body'></tbody></table></body>");
    //     1) get all dates of trainings in period 
    let request_data= {type:"get_dates_list",session:sessionid,slot_id:training_slot,list_begin:training_from,list_end:training_to}
    post_server(request_data).then(function(response){
        let data = response;
        decodeURI(data);
        if(correct_query(data)){
            for(let i=0;i<data.length;i++){
                let part = data[i];
                let thead = list_window.document.getElementById("header");
                let date = list_window.document.createElement("TD");
                date.classList.add("rotate");
                date.innerHTML="<div><span>"+part["date"]+"</span></div>";
                thead.appendChild(date);
                id_all.push(part["id"]);
            }
        }
    }).then(function(){
        let request_data_new = {type:"get_judoka_trainings",session:sessionid,slot_id:training_slot,list_begin:training_from,list_end:training_to}
        post_server(request_data_new).then(function(response){
            let data = response;
            decodeURI(data);
            if(correct_query(data)){
                for(let i=0;i<data.length;i++){
                    part = data[i];
                    let trainings_ids = part["trainings"];
                    let trainings_array = trainings_ids.split(",");
                    let tbody = list_window.document.getElementById("body");
                    let row = list_window.document.createElement("TR");
                    if(part["grade"]<0){
                        row.innerHTML="<td>"+decodeURIComponent(part["prename"]) + " " + decodeURIComponent(part["name"])+"</td><td class='date'>"+part["birth_date"]+"</td><td class='date'>"+Math.abs(part["grade"])+".Dan</td>";
                        for(let j = 0; j<id_all.length;j++){
                            if(trainings_array.includes(id_all[j])){
                                row.innerHTML+="<td class='date'>X</td>";
                            }else{
                                row.innerHTML+="<td class='date'>-</td>";
                            }
                        }
                    }else{
                        row.innerHTML="<td>"+decodeURIComponent(part["prename"]) + " " + decodeURIComponent(part["name"])+"</td><td class='date'>"+part["birth_date"]+"</td><td class='date'>"+part["grade"]+".Kyu</td>";
                        for(let j = 0; j<id_all.length;j++){
                            if(trainings_array.includes(id_all[j])){
                                row.innerHTML+="<td class='date'>X</td>";
                            }else{
                                row.innerHTML+="<td class='date'>-</td>";
                            }
                        }
                    }
                    tbody.appendChild(row);
                }
            }
            $('#dialog_print_training').modal('toggle');
        });
    });
}

function print_training_empty(){
    table_length = 0;
    list_window=window.open();
    list_window.blur();
    let training_slot = document.getElementById("input_list_slot").value;
    let training_from = document.getElementById("input_list_from").value;
    let training_to = document.getElementById("input_list_to").value;
    table_length = 20;
    set_training_template(list_window);
    list_window.document.write("<body><h2>"+$("#input_list_slot").find('option:selected').text()+"</h2><table><tr id='header' class='firstrow'><th>Schüler</th><th class='rotate'><div><span>Geburtsdatum</span></div></th><th class='rotate'><div><span>Graduierung</span></div></th></tr></thead><tbody id='body'></tbody></table></body>");
    for(let i=0;i<table_length;i++){
        let thead = list_window.document.getElementById("header");
        let date = list_window.document.createElement("TD");
        date.classList.add("rotate");
        date.innerHTML="<div><span></span></div>";
        thead.appendChild(date);
    }
    let request_data= {type:"get_pupils_empty",session:sessionid,slot_id:training_slot,list_begin:training_from,list_end:training_to}
    post_server(request_data).then(function(response){
        let data = response;
        decodeURI(data);
        if(correct_query(data)){
            for(let i=0;i<data.length;i++){
                let part = data[i];
                let tbody = list_window.document.getElementById("body");
                let row = list_window.document.createElement("TR");
                if(part["grade"]<0){
                    row.innerHTML="<td>"+decodeURIComponent(part["prename"]) + " " + decodeURIComponent(part["name"])+"</td><td class='date'>"+part["birth_date"]+"</td><td class='date'>"+Math.abs(part["grade"])+".Dan</td>";
                    for(let j = 0; j<table_length;j++){
                    row.innerHTML+="<td class='date'></td>";
                    }
                }else{
                    row.innerHTML="<td>"+decodeURIComponent(part["prename"]) + " " + decodeURIComponent(part["name"])+"</td><td class='date'>"+part["birth_date"]+"</td><td class='date'>"+part["grade"]+".Kyu</td>";
                    for(let j = 0; j<table_length;j++){
                        row.innerHTML+="<td class='date'></td>";
                    }
                }
                tbody.appendChild(row);
            }
        }
        $('#dialog_print_training').modal('toggle');
    });
}

function print_exam_template_csv(){
    let selected_div = document.querySelector('.selected_row.exam');
    if(selected_div){
        request_data = {type:"get_exam_list", session:sessionid,id:selected_div.id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let csvContent = "data:text/csv;charset=utf-8,";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                let prevGrade = "";
                if(part["received_grade"] == "8"){
                    prevGrade = "-";
                }else if(part["received_grade"] == "-1"){
                    prevGrade = "1";
                }else if(parseInt(part["received_grade"]) > 0){
                    prevGrade = parseInt(part["received_grade"])+1;
                }else if (parseInt(part["received_grade"]) < 0){
                    prevGrade = parseInt(part["received_grade"])-1;
                }
                csvContent = csvContent + part["name"] + ",," + part["prename"] + ",,,,,,,," + part["pass"] + ",,," + part["birth_date"]+ ",," + part["last_date_exam"] + ",," + prevGrade + ",,,,,,,,," + part["received_grade"] + "\n";
            }
            let uriEncoded = encodeURI(csvContent);
            link = document.createElement('a');
            link.setAttribute('href', uriEncoded);
            link.setAttribute('download', 'pruefungsliste.csv');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        });
    }else{display_alert("nothing selected");}
}
