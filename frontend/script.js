var URL = 'https://svjudo.goip.de:8081';

var sessionid = "0";
var output = document.getElementById("output");
var text = "";
var admin = false;

var loading_modal = $('#dialog_loading');

loading_modal.modal({
            'backdrop': 'static',
            'keyboard': false,
            'show':     false
          });

var login_modal = $('#dialog_during_login');

login_modal.modal({
            'backdrop': 'static',
            'keyboard': false,
            'show':     false
          });

//sortTable based closely on https://www.w3schools.com/howto/howto_js_sort_table.asp

function sortTable(table_id, n) {
    for(let k=0; k<4;k++){
        if(k!=n){
            document.getElementById("pictogram"+k).innerHTML="&#9670;";
        }
    }
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(table_id);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc"; 
    document.getElementById("pictogram"+n).innerHTML="&#9660;";
    //no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
         *   first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
             *     one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
             *     based on the direction, asc or desc: */
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
             *       and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++; 
        } else {
            /* If no switching has been done AND the direction is "asc",
             *           set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                document.getElementById("pictogram"+n).innerHTML="&#9650;";
                switching = true;
            }
        }
    }
}

function display_alert(reason){
    $(".modal.fade.in").modal('hide');
    document.getElementById("alerttext").innerHTML = reason;
    document.getElementById("alertwindow").classList.remove('nonvisible');
}

function alert_no_login(object){
    if(object["sessionid"] == "0"){
        display_alert("nicht eingeloggt:" + object["reason"]);
        clear_output();
        $("#dialog_login").modal('show');
    }
}

function decodeURI(object){
    for(let i in object){
        if(object.hasOwnProperty(i)){
            for(let key in object[i]){
                if (object[i].hasOwnProperty(key)) {
                    object[i][key] = decodeURIComponent(object[i][key])
                }
            }
        }
    }
}

function alert_no_success(object){
    if(object["success"] == "0"){
        display_alert("Fehler beim Schreiben in Datenbank.\n " + object["reason"]);
    }
}

function correct_query(object){
    if(object["empty"] == "1"){
        console.log("leeres Query-Result");
        return false;
    }else{
        return true;
    }
}

function clear_dialog(dialog){
    let output = document.getElementById(dialog);
    if(dialog == "dialog_add_judoka_csv"){
        document.getElementById("csv_output").innerHTML = "";
    }
    if(dialog != "dialog_show_userinfo" && dialog != "dialog_logout"){
        let form = output.getElementsByTagName("FORM");
        for(i=0; i<form[0].children.length; i++){
            if(form[0].children[i].nodeName=="INPUT" || form[0].children[i].nodeName=="SELECT"){
                if(form[0].children[i].type!="button"){
                    form[0].children[i].value="";
                }
                form[0].children[i].checked=false;
            }
            if(form[0].children[i].nodeName=="SELECT"){
                for(j=form[0].children[i].children.length-1; j>-1; j--){
                    form[0].children[i].children[j].remove();
                }
            }
            if(form[0].children[i].nodeName=="TEXTAREA"){
                form[0].children[i].value="";
            }
        }
    }else if(dialog == "dialog_show_userinfo"){
        let p = output.getElementsByTagName("P");
        for(i=0; i<p.length; i++){
            p[i].innerHTML="";
        }
    }
    output.style.display="none";
}

function count_participants(string_participants){
    let count = (string_participants.match(/,/g) || []).length;
    return count +1;
}

function clear_output(){
    document.getElementById("output").innerHTML="";  
}

function parse_bool(bool){
    if(bool){
        return 1;
    }else{
        return 0;  
    }
}

function post_server(request_data){
    return new Promise(function(resolve,reject){
        $.ajax({
            type: 'POST',
            url: URL,
            crossDomain: true,
            data: request_data,
            dataType: 'json',
            success: function(responseData, textStatus, jqXHR) {
                resolve(responseData);
            },
            error: function (responseData, textStatus, errorThrown) {
                if(textStatus=="error"){
                    $('.modal.loading').modal('hide');
                    display_alert('Error communicating with backend. Timeout.');
                    
                }
                if(responseData){
                    if(responseData.status==401){
                        $('.modal.loading').modal('hide');
                        display_alert("unauthorized: username or password don't match");
                    }else if(responseData.status==500){
                        error = JSON.parse(responseData.responseText)['reason'];
                        $('.modal.loading').modal('hide');
                        display_alert("Error returned by backend: " + error);
                    }
                }else{
                    $('.modal.loading').modal('hide');
                    display_alert('Error communicating with backend.');
                }
            }
        });
    });
}

function concat_options(id){
    let options = document.getElementById(id).options;
    let result = ""
    for(let i = 0; i<options.length; i++){
        if(options[i].selected){
            result += options[i].value + " ";
        }
    }
    //remove leading and training spaces.
    result = result.trim()
    return result;
}

function alter_classes(class0, class1){
    let divs_delete = document.getElementsByClassName(class0);
    for(let m=divs_delete.length-1; m>-1; m--){
        divs_delete.item(m).classList.add(class1);
        divs_delete.item(m).classList.remove(class0);
    } 
}

function clear_selected(){
    let divs_delete = document.getElementsByClassName("selected_row");
    for(let m=divs_delete.length-1; m>-1; m--){
        divs_delete.item(m).classList.remove("selected_row");
    }  
}

function set_admin_layout(isAdmin){
    if(isAdmin){
        admin=true;
        document.getElementById("adminli").style.display="block";
    }else{
        admin=false;
        document.getElementById("adminli").style.display="none";
    }
}

function clicked(e){
    let clicked = e.target;
    if(clicked.nodeName == "TD"){
        clear_selected();
        clicked.parentElement.classList.add("selected_row");
    }else if(clicked.nodeName == "TR"){
        clear_selected();
        clicked.classList.add("selected_row");
    }else if (clicked.nodeName == "DIV"){
        if(clicked.classList.contains("comp")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("exam")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("training")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("license")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("slot")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("user")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("group")){
            clear_selected();
            clicked.classList.add("selected_row");
        }else if(clicked.classList.contains("request")){
            clear_selected();
            clicked.classList.add("selected_row");
        }
    }else if(clicked.nodeName =="SPAN"){
        if(clicked.classList.contains("training")){
            clear_selected();
            clicked.parentElement.classList.add("selected_row")
        }
    }else if(clicked.nodeName =="TH"){
        if(clicked.classList.contains("0")){
            sortTable('table_judoka',0);
        }else if(clicked.classList.contains("1")){
            sortTable('table_judoka',1);
        }else if(clicked.classList.contains("2")){
            sortTable('table_judoka',2);
        }else if(clicked.classList.contains("3")){
            sortTable('table_judoka',3);
        }
    }else{
        clear_selected();
        console.log(clicked.nodeName);
    }    
    if(clicked.classList.contains("open")){
        clicked.classList.remove("open");
        clicked.innerHTML='-';
        clicked.classList.add("opened");
        for( let m=clicked.parentElement.children.length-1; m>-1; m--){
            if(clicked.parentElement.children.item(m).classList.contains("hidden")){
                clicked.parentElement.children.item(m).classList.add("shown");
                clicked.parentElement.children.item(m).classList.remove("hidden");
            }
        }
    }else if(clicked.classList.contains("opened")){
        clicked.classList.remove("opened");
        clicked.innerHTML='+';
        clicked.classList.add("open");
        for( let m=clicked.parentElement.children.length-1; m>-1; m--){
            if(clicked.parentElement.children.item(m).classList.contains("shown")){
                clicked.parentElement.children.item(m).classList.add("hidden");
                clicked.parentElement.children.item(m).classList.remove("shown");
            }
        }
    }
    if(clicked.classList.contains("append")){
        clicked.classList.remove("append");
        clicked.innerHTML='-';
        clicked.classList.add("appended");
        alter_classes("clipped", "unclipped");
    }else if(clicked.classList.contains("appended")){
        clicked.classList.remove("appended");
        clicked.innerHTML='+';
        clicked.classList.add("append");
        alter_classes("unclipped", "clipped");
    }
}

function add_judoka(){
    let judoka_name = document.getElementById("input_name").value;
    let judoka_prename = document.getElementById("input_prename").value;
    let judoka_grade = document.getElementById("input_grade").value;
    let judoka_birthdate = document.getElementById("input_birthdate").value;
    let judoka_contact = document.getElementById("input_contact").value;
    let judoka_wkl = document.getElementById("input_wkl").value;
    let judoka_pass_nr = document.getElementById("input_pass_nr").value;
    let judoka_examiner = parse_bool(document.getElementById("input_examiner_license").checked);
    let judoka_dsgvo = parse_bool(document.getElementById("input_dsgvo").checked);
    let judoka_comment = document.getElementById("input_comment").value;
    let judoka_licenses = concat_options("input_license");
    
    let request_data = {type:"add_judoka", session:sessionid,judoka_name:judoka_name, judoka_prename:judoka_prename, judoka_graduation:judoka_grade, judoka_birthdate:judoka_birthdate,judoka_contact:judoka_contact,judoka_wkl:judoka_wkl,judoka_pass:judoka_pass_nr,judoka_examiner:judoka_examiner,judoka_dsgvo:judoka_dsgvo,judoka_comment:judoka_comment,judoka_license:judoka_licenses};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_add_judoka").modal('toggle');
    show_judoka();
}

function edit_judoka(){
    let judoka_id = document.querySelector('.selected_row.judoka').id;
    let judoka_name = document.getElementById("input_name_edit").value;
    let judoka_prename = document.getElementById("input_prename_edit").value;
    let judoka_grade = document.getElementById("input_grade_edit").value;
    let judoka_birthdate = document.getElementById("input_birthdate_edit").value;
    let judoka_contact = document.getElementById("input_contact_edit").value;
    let judoka_wkl = document.getElementById("input_wkl_edit").value;
    let judoka_pass_nr = document.getElementById("input_pass_edit").value;
    let judoka_examiner = parse_bool(document.getElementById("input_examiner_license_edit").checked);
    let judoka_dsgvo = parse_bool(document.getElementById("input_dsgvo_edit").checked);
    let judoka_comment = document.getElementById("input_comment_edit").value;
    let judoka_licenses = concat_options("input_license_edit");    
    let request_data = {type:"edit_judoka", session:sessionid,judoka_id:judoka_id, judoka_name:judoka_name, judoka_prename:judoka_prename, judoka_graduation:judoka_grade, judoka_birthdate:judoka_birthdate,judoka_contact:judoka_contact,judoka_wkl:judoka_wkl,judoka_pass:judoka_pass_nr,judoka_examiner:judoka_examiner,judoka_dsgvo:judoka_dsgvo,judoka_comment:judoka_comment,judoka_license:judoka_licenses};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
        $("#dialog_edit_judoka").modal('toggle');
        show_judoka();
    });
}

function show_judoka(){
    clear_output();
    const request_data = {type:"show_judoka_sort", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "<button class='btn append'>+</button><table id='table_judoka'> <thead><th class='0' >Name <span id='pictogram0'>&#9670;</span></th><th class='1'>Vorname <span id='pictogram1'>&#9660;</span></th><th class='2'>Geburtsdatum <span id='pictogram2'>&#9670;</span></th> <th class='3'>Graduierung <span id='pictogram3'>&#9670;</span></th> <th class='clipped'>Passnummer</th>  <th class='clipped'>Kontakt</th> <th class='clipped'>Wettkampflizenz</th> <th class='clipped'>Photo</th> <th class='clipped'>Prüfer</th> <th class='clipped'>Lizenzen</th>   <th class='clipped'> Kommentar</th></thead><tbody id='tablebody'>"
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                let append = "";
                if(part["grade"]>0){
                    append = "<tr id=\""+ part["id"]+"\" class='judoka'><td>"+ part["name"]+"</td><td>"+ part["prename"]+"</td><td>"+ part["birth_date"]+"</td> <td>"+ part["grade"]+". Kyu</td> <td class='clipped'>"+ part["pass"]+"</td>  <td class='clipped'>"+ decodeURIComponent(part["contact"])+"</td><td class='clipped'>"+ part["wkl"]+"</td> <td class='clipped'>"+ part["dsgvo_photo"]+"</td> <td class='clipped'>"+part["examiner"]+"</td> <td class='clipped' id='lic-"+part["id"]+"'>" + part["license"] +"</td> <td class='clipped'>"+ decodeURIComponent(part["comment"])+"</td></tr>"
                }else{
                    append = "<tr id=\""+ part["id"]+"\" class='judoka'><td>"+ part["name"]+"</td><td>"+ part["prename"]+"</td><td>"+ part["birth_date"]+"</td> <td>"+ Math.abs(part["grade"])+". Dan</td> <td class='clipped'>"+ part["pass"]+"</td>  <td class='clipped'>"+ decodeURIComponent(part["contact"])+"</td><td class='clipped'>"+ part["wkl"]+"</td> <td class='clipped'>"+ part["dsgvo_photo"]+"</td> <td class='clipped'>"+part["examiner"]+"</td> <td class='clipped' id='lic-"+part["id"]+"'> " + part["license"] +" </td> <td class='clipped'>"+ decodeURIComponent(part["comment"])+"</td></tr>"
                }
                text+=append;
            }
            text +=  '</tbody></table>';
            output.innerHTML = text;
        }
    });
}

function show_judoka_trainings(){
    let selected_div = document.querySelector('.selected_row.judoka');
    let id = selected_div.id;
    clear_output();
    const request_data = {type:"judoka_get_training", session:sessionid, judoka_id:id};
    post_server(request_data).then(function(response){
        let data = response;
        decodeURI(data);
        alert_no_login(data);
        if(correct_query(data)){
            let text="";
            for(let i = 0; i<data.length; i++){
                let part = data[i];
                let id = part["trainingid"];
                let slot_id = part["slotid"];
                let name = part["name"];
                let date=part["date"];
                let plan = part["plan"];
                let trainer = part["trainer"]
                let comment = part["comment"];
                let slot_div = document.getElementById("slot"+slot_id);
                if(!slot_div){
                    slot_div=document.createElement("DIV");
                    slot_div.id="slot"+slot_id;
                    slot_div.innerHTML="<span class='open'>+</span> "+name;
                    output.appendChild(slot_div);
                }
                let training_div = document.createElement("DIV");
                training_div.id="training"+id;
                training_div.classList.add("hidden","indent","training");
                training_div.innerHTML=date+": <span id='trainer"+id+"' class='training'> "+ trainer +" </span> <div class='indent hidden'> <h3>Plan:</h3><p>"+plan+"</p></div><div class='indent hidden'><h3>Kommentar:</h3><p>"+comment+"</p></div>";
                slot_div.appendChild(training_div);
            }
        }
    });
}

function show_judoka_exams(){
    let selected_div = document.querySelector('.selected_row.judoka');
    let id = selected_div.id;
    clear_output();
    const request_data = {type:"judoka_get_exam", session:sessionid, judoka_id:id};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "";
            for(let i = 0; i<data_obj.length; i++){
                let part=data_obj[i];
                if(part["received_grade"] <0){
                    text += "<div id='" + part["id"] + "'> "+ part["date"] + " (" + Math.abs(part["received_grade"]) +". Dan ) ["+part["examiner"]+"]</div>";
                }else{
                    text += "<div id='" + part["id"] + "'> "+ part["date"] + " (" + part["received_grade"] +". Kyu ) ["+part["examiner"]+"]</div>";
                }
            }
            output.innerHTML = text;
        }
    });
}

function show_judoka_comp(){
    let selected_div = document.querySelector('.selected_row.judoka');
    let id = selected_div.id;
    clear_output();
    console.log(selected_div);
    const request_data = {type:"judoka_get_comp", session:sessionid, judoka_id:id};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "";
            for(let i = 0; i<data_obj.length; i++){
                let part=data_obj[i];
                console.log(part);
                text += "<div id='" + part["id"] + "'> "+ part["name"] + " (" + part["location"] + "," + part["date"] + "):" + part["class"] + " " + part["result"] +" (" + part["comment"] + ")</div>"; 
            }
            output.innerHTML = text;
        }
    });
}

function show_training(){
    clear_output();
    let request_data = {type:"show_training", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text="";
            for(let i = 0; i<data_obj.length; i++){
                let part = data_obj[i];
                let id = part["id"];
                let slot_id = part["slot_id"];
                let name = part["name"];
                let date=part["date"];
                let plan = part["plan"];
                let comment = part["comment"];
                let slot_div = document.getElementById("slot"+slot_id);
                if(!slot_div){
                    slot_div=document.createElement("DIV");
                    slot_div.id="slot"+slot_id;
                    slot_div.innerHTML="<button class='btn open'>+</button> "+name;
                    output.appendChild(slot_div);
                }
                let training_div = document.createElement("DIV");
                training_div.id=id;
                training_div.classList.add("hidden","indent","training");
                training_div.innerHTML="<button class='btn open'>+</button> "+date+": <span id='trainer"+id+"' class='training'>"+part["trainer"]+"</span> ("+ count_participants(part["participants"]) + ")<div class='indent hidden'> <h5>Plan:</h5><p>"+plan+"</p></div><div class='indent hidden'><h5>Kommentar:</h5><p>"+comment+"</p></div><h5 class='indent hidden'> Anwesend: </h5><div id='participants"+id+"' class='indent hidden'> "+part["participants"]+"</div>";
                slot_div.appendChild(training_div);
            }
        }
    });
}

function edit_training(){
    let training_id = document.querySelector('.selected_row.training').id;
    let training_date = document.getElementById("input_training_date_edit").value;
    let training_slot = document.getElementById("input_slot_edit").value;
    let training_pupils = concat_options("input_pupils_edit");
    let trainers = concat_options("input_trainer_edit");
    let training_plan = document.getElementById("input_plan_edit").value;
    let training_comment = document.getElementById("input_comment_training_edit").value;
    let request_data = {type:"edit_training", session:sessionid,id:training_id,date:training_date, training_slot:training_slot, pupils:training_pupils,trainer:trainers,plan:training_plan,training_comment:training_comment};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_edit_training").modal('toggle');
    show_training();
}

function add_training(){
    let training_date = document.getElementById("input_training_date").value;
    let training_slot = document.getElementById("input_slot").value;
    let training_pupils = concat_options("input_pupils");
    let trainers = concat_options("input_trainer");
    let training_plan = document.getElementById("input_plan").value;
    let training_comment = document.getElementById("input_comment_training").value;
    let request_data = {type:"add_training", session:sessionid,date:training_date, training_slot:training_slot, pupils:training_pupils,trainer:trainers,plan:training_plan,training_comment:training_comment};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_add_training").modal('toggle');
    show_training();
}

function add_comp(){
    let date = document.getElementById("input_comp_date").value;
    let name = document.getElementById("input_comp_name").value;
    let coach = concat_options("input_coach");
    let location = document.getElementById("input_location").value;
    let result = "";
    for(let i = 0; i<10;i++){
        result += document.getElementById("input_fighter"+ i).value + "$" + document.getElementById("input_class"+ i).value + "$"+ document.getElementById("input_result"+ i).value +"$"+ document.getElementById("input_comment"+ i).value +"$";
    }
    let request_data = {type:"add_comp", session:sessionid,date:date, comp_name:name, coach:coach,location:location,results:result};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_add_comp").modal('toggle');
    show_comp();
}

function edit_comp(){
    let comp_id = document.querySelector('.selected_row.comp').id;
    let date = document.getElementById("input_comp_date_edit").value;
    let name = document.getElementById("input_comp_name_edit").value;
    let coach = concat_options("input_coach_edit");
    let location = document.getElementById("input_location_edit").value;
    let result = "";
    for(let i = 0; i<10;i++){
        result += document.getElementById("input_fighter"+ i+"_edit").value + "$" + document.getElementById("input_class"+ i+"_edit").value + "$"+ document.getElementById("input_result"+ i+"_edit").value +"$"+ document.getElementById("input_comment"+ i+"_edit").value +"$";
    }
    let request_data = {type:"edit_comp", id:comp_id, session:sessionid,date:date, comp_name:name, coach:coach,location:location,results:result};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_edit_comp").modal('toggle');
    show_comp();
}

function show_comp(){
    clear_output();
    let request_data = {type:"show_comp", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text="";
            for(let i = 0; i<data_obj.length; i++){
                let part = data_obj[i];
                let id = part["id"];
                let location = part["location"];
                let name = part["name"];
                let date=part["date"];;
                let slot_div=document.createElement("DIV");
                slot_div.id=id;
                slot_div.classList.add("comp");
                slot_div.innerHTML="<button class='btn open'>+</button> "+name + "("+location+ ","+ date+")";
                output.appendChild(slot_div);
                let training_div = document.createElement("DIV");
                training_div.id="data_comp"+id;
                training_div.classList.add("hidden","indent");
                training_div.innerHTML="<p id='coach"+id+"'></p><p id='participants"+id+"' ></p>";
                slot_div.appendChild(training_div);
                request_data = {type:"get_coach_comp", session:sessionid,id:id};
                post_server(request_data).then(function(response){
                    let data = response;
                    decodeURI(data);
                    if(correct_query(data)){
                        let trainer_span = document.getElementById("coach"+data[0]["competition_id"]);
                        trainer_span.innerHTML+="<b>Betreuung: </b>";
                        for(let i = 0; i<data.length;i++){
                            trainer_span.innerHTML+=data[i]["prename"]+ " " + data[i]["name"] + ",";
                        }
                    }
                });
                request_data = {type:"get_comp_participants", session:sessionid,id:id};
                post_server(request_data).then(function(response){
                    let data = response;
                    decodeURI(data);
                    if(correct_query(data)){
                        let trainer_span = document.getElementById("participants"+data[0]["competition_id"]);
                        for(let i = 0; i<data.length;i++){
                            if(data[i]["result"] == "-1"){
                                trainer_span.innerHTML+=data[i]["prename"]+ " " + data[i]["name"] + ", " + data[i]["class"]+ ", teilg. ;" + data[i]["comment"]+"<br/>";
                            }else{
                                trainer_span.innerHTML+=data[i]["prename"]+ " " + data[i]["name"] + ", " + data[i]["class"]+ ", " + data[i]["result"] + ";" + data[i]["comment"]+"<br/>";
                            }
                        }
                    }
                });
            }
        }
    });
}

function add_exam(){
    let date = document.getElementById("input_date_exam").value;
    let begin = document.getElementById("input_begin_exam").value;
    let end = document.getElementById("input_end_exam").value;
    let duration = document.getElementById("input_duration_exam").value;
    let examiner = concat_options("input_examiner");
    let examined = "";
    for(let i = 0; i<20;i++){
        examined += document.getElementById("input_examined"+ i).value + " " + document.getElementById("input_grade_received"+ i).value + " ";
    }
    let request_data = {type:"add_exam", session:sessionid,date:date, examiner:examiner,examined:examined,begin:begin,end:end,duration:duration};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_add_exam").modal('toggle');
    show_exam();
}

function edit_exam(){
    let exam_id = document.querySelector('.selected_row.exam').id;
    let date = document.getElementById("input_date_exam_edit").value;
    let begin = document.getElementById("input_begin_exam_edit").value;
    let end = document.getElementById("input_end_exam_edit").value;
    let duration = document.getElementById("input_duration_exam_edit").value;
    let examiner = concat_options("input_examiner_edit");
    let examined = "";
    for(let i = 0; i<20;i++){
        examined += document.getElementById("input_examined"+ i +"_edit").value + " " + document.getElementById("input_grade_received"+ i +"_edit").value + " ";
    }
    let request_data = {type:"edit_exam", session:sessionid,date:date, examiner:examiner,examined:examined,id:exam_id,begin:begin,end:end,duration:duration};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_edit_exam").modal('toggle');
    show_exam();
}

function show_exam(){
    clear_output();
    let request_data = {type:"show_exam",session:sessionid}
    post_server(request_data).then(function(response){
        let data = response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            let text = "";
            for(let i = 0; i<data.length; i++){
                let part = data[i];
                let id = part["id"];
                let div_comp = document.createElement("DIV");
                div_comp.id=part["id"];
                div_comp.classList.add("exam");
                div_comp.innerHTML= "<button class='btn open'>+</button><span id='exam_date" +part["id"] +"'>" + part["date"] + "</span>, <span id='exam_begin" +part["id"] +"'>" + part["begin"] + "</span> - <span id='exam_end" +part["id"] +"'>" + part["end"] + "</span> (<span id='exam_duration" +part["id"] +"'>" + part["duration"] + "</span> min.)  [<span id='examiner"+part["id"]+"'>"+part["examiner"]+"</span>] <div id='participants"+part["id"]+ "' class='indent hidden'>"+part["examined"].replace(/;/g, "<br/>")+"</div>";
                output.appendChild(div_comp);
            }
        }
    });
}


function login(){
    console.log("initial login");
    console.log($('body'));
    let input_username = document.getElementById("input_username").value;
    let input_pass = document.getElementById("input_pass").value;
    $("#dialog_login").modal('hide').on('hidden.bs.modal', function (e) {
        $(this).off('hidden.bs.modal');
        $("#dialog_during_login").modal('show');
        const request_data = {type:"login", username:input_username,pass:input_pass};
        post_server(request_data).then(function(response){
            let data_obj = response;
            sessionid = data_obj["sessionid"];
            if(sessionid == "0"){
                $("#dialog_during_login").modal('hide');
                display_alert("nicht eingeloggt, womöglich falsches Passwort?");
            }else{
                let admin = data_obj["admin"];
                if(admin == "1"){
                    set_admin_layout(true);
                }
                document.getElementById("alertwindow").classList.add('nonvisible');
                $("#dialog_during_login").modal('hide');
            }
        });
    });
}

function logout(){
    const request_data = {type:"logout", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        console.log(data_obj);
        success = data_obj["success"];
        sessionid = data_obj["sessionid"];
        if(sessionid == "0"){ //current session is no longer valid
            display_alert("Session abgelaufen");
            clear_output();
            set_admin_layout(false);
            $("#dialog_logout").modal('toggle');
        }else{
            if(success == "1"){ //current session killed
                sessionid = "0";
                set_admin_layout(false);
                $("#dialog_logout").modal('toggle');
                clear_output();
            }else{
                display_alert("nicht ausgeloggt");
            }
        };
    });
}

function edit_user(){
    let username = document.getElementById("input_username_edit").value;
    let pass_new = document.getElementById("input_pass_new").value;
    let pass_validate = document.getElementById("input_pass_new_confirm").value;
    if(pass_new == "" || !pass_new){
        display_alert("password must be set");
    }
    if(pass_new != pass_validate){
        display_alert("password mismatch");
    }else{
        let request_data = {type:"edit_user", session:sessionid,username:username,pass:pass_new};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_user").modal('toggle');
    }
}

function submit_request(){
    let request_body = document.getElementById("input_request").value;
    let request_data = {type:"submit_request", session:sessionid,request:request_body};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_request").modal('toggle');
    show_requests();
}

function delete_training(){
    let selected_div = document.querySelector('.selected_row.training');
    const request_data = {type:"delete_training", session:sessionid, id:selected_div.id};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_edit_training").modal('toggle');
    show_training();
}

function delete_user_admin(){
    if(admin){
        let selected_div = document.querySelector('.selected_row.user');
        const request_data = {type:"delete_user", session:sessionid, id:selected_div.id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_user_admin").modal('toggle');
        show_user();
    }else{display_alert("User is not Admin!")};
}

function delete_group(){
    if(admin){
        let selected_div = document.querySelector('.selected_row.group');
        const request_data = {type:"delete_group", session:sessionid, id:selected_div.id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_group").modal('toggle');
        show_groups();
    }else{display_alert("User is not Admin!")};
}

function delete_slot(){
    if(admin){
        let selected_div = document.querySelector('.selected_row.slot');
        const request_data = {type:"delete_slot", session:sessionid, id:selected_div.id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_slot").modal('toggle');
        show_slots();
    }else{display_alert("User is not Admin!")};
}

function delete_judoka(){
    let selected_div = document.querySelector('.selected_row.judoka');
    if(selected_div){
        display_alert("Judoka is going to be deleted.")
        const request_data = {type:"delete_judoka", session:sessionid, id:selected_div.id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
            show_judoka();
        });
    }else{display_alert("no Judoka selected!");}
}

function delete_exam(){
    let selected_div = document.querySelector('.selected_row.exam');
    const request_data = {type:"delete_exam", session:sessionid, id:selected_div.id};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_edit_exam").modal('toggle');
    show_exam();
}

function delete_comp(){
    let selected_div = document.querySelector('.selected_row.comp');
    const request_data = {type:"delete_comp", session:sessionid, id:selected_div.id};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        alert_no_success(data_obj);
    });
    $("#dialog_edit_comp").modal('toggle');
    show_comp();
}

function show_requests(){
    clear_output();
    const request_data={type:"show_requests",session:sessionid};
    post_server(request_data).then(function(response){
        let data = response;
        alert_no_login(data);
        decodeURI(data);
        alert_no_success(data);
        if(correct_query(data)){
            for(let i=0; i<data.length;i++){
                let part = data[i];
                let request_div = document.createElement("DIV");
                request_div.id=part["id"];
                request_div.classList.add("request");
                request_div.innerHTML="<button class='btn open'>+</button>"+part["datetime_issued"] + " (" + part["done"]+") <div class='indent hidden'> "+part["text"]+"</div>" 
                output.appendChild(request_div);
            }
        }
    });
}

function show_requests_admin(){
    clear_output();
    if(admin){
        const request_data={type:"show_requests_admin",session:sessionid};
        post_server(request_data).then(function(response){
            let data = response;
            alert_no_login(data);
            alert_no_success(data);
            decodeURI(data);
            if(correct_query(data)){
                for(let i=0; i<data.length;i++){
                    let part = data[i];
                    let request_div = document.createElement("DIV");
                    request_div.id=part["id"];
                    request_div.classList.add("request");
                    request_div.innerHTML="<button class='btn open'>+</button>"+part["datetime_issued"] + " [" + part["username"]+"]  (" + part["done"]+") <div class='indent hidden'> "+part["text"]+"</div>" 
                    output.appendChild(request_div);
                }
            }
        });
    }else{display_alert("User is not Admin!")};
}

function mark_as_done(){
    if(admin){
        let selected_div = document.querySelector('.selected_row.request');
        const request_data = {type:"mark_request", session:sessionid, id:selected_div.id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
            show_requests_admin();
        });
    }else{display_alert("User is not Admin!")};
}

function add_group(){
    if(admin){
        let name = document.getElementById("input_groupname").value;
        let members = concat_options("input_members");
        let readable = concat_options("input_readable");
        let request_data = {type:"add_group", session:sessionid,name:name,members:members,readable:readable};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_add_group").modal('toggle');
        show_groups();
    }else{display_alert("User is not Admin!")};
}

function edit_user_group(){
    if(admin){
        let id = document.querySelector(".selected_row.user").id;
        let groups = concat_options("input_group_edit");
        let request_data = {type:"edit_user_group", session:sessionid,groups:groups, id:id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_user_admin").modal('toggle');
        show_user();
    }else{display_alert("User is not Admin!")};
}

function add_user(){
    if(admin){
        let username = document.getElementById("input_username_admin").value;
        let groups = concat_options("input_group");
        let judoka = document.getElementById("input_assoc_judoka").value;
        let pass = document.getElementById("input_pass_admin").value;
        let request_data = {type:"add_user", session:sessionid,username:username,groups:groups,judoka_id:judoka,pass:pass};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_add_user").modal('toggle');
        show_user();
    }else{display_alert("User is not Admin!")};
}

function edit_user_admin(){
    if(admin){
        let id = document.querySelector(".selected_row.user").id;
        let username = document.getElementById("input_username_edit_admin").value;
        let groups = concat_options("input_group_edit");
        let judoka = document.getElementById("input_assoc_judoka_edit").value;
        let pass = document.getElementById("input_pass_new_admin").value;
        let request_data = {type:"edit_user_admin", session:sessionid,username:username,groups:groups,judoka_id:judoka, id:id,pass:pass};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_user_admin").modal('toggle');
        show_user();
    }else{display_alert("User is not Admin!")};
}

function edit_group(){
    if(admin){
        let id = document.querySelector(".selected_row.group").id;
        let name = document.getElementById("input_groupname_edit").value;
        let members = concat_options("input_members_edit");
        let readable = concat_options("input_readable_edit");
        let request_data = {type:"edit_group", session:sessionid,name:name,members:members,readable:readable,id:id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_group").modal('toggle');
        show_groups();
    }else{display_alert("User is not Admin!")};
}

function add_slot(){
    if(admin){
        let name = document.getElementById("input_slotname").value;
        let begin = document.getElementById("input_begin").value;
        let end = document.getElementById("input_end").value;
        let duration = document.getElementById("input_duration").value;
        let rate = document.getElementById("input_rate_slot").value;
        let active = parse_bool(document.getElementById("input_slot_active").checked);
        
        let request_data = {type:"add_slot", session:sessionid,name:name,begin:begin,end:end,duration:duration,rate:rate,active:active};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_add_slot").modal('toggle');
        show_slots();
    }else{display_alert("User is not Admin!")};
}

function edit_slot(){
    if(admin){
        let id = document.querySelector(".selected_row.slot").id;
        let name = document.getElementById("input_slotname_edit").value;
        let begin = document.getElementById("input_begin_edit").value;
        let end = document.getElementById("input_end_edit").value;
        let duration = document.getElementById("input_duration_edit").value;
        let rate = document.getElementById("input_rate_slot_edit").value;
        let active = parse_bool(document.getElementById("input_slot_active_edit").checked);
        
        let request_data = {type:"edit_slot", session:sessionid,name:name,begin:begin,end:end,id:id,duration:duration,rate:rate,active:active};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_slot").modal('toggle');
        show_slots();
    }else{display_alert("User is not Admin!")};
}

function add_license(){
    if(admin){
        let name = document.getElementById("input_license_name").value;
        let rate = document.getElementById("input_license_rate").value;
        let request_data = {type:"add_license", session:sessionid,name:name,rate:rate};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_add_license").modal('toggle');
        show_licenses();
    }else{display_alert("User is not Admin!")};
}

function edit_license(){
    if(admin){
        let id = document.querySelector(".selected_row.license").id;
        let name = document.getElementById("input_license_name_edit").value;
        let rate = document.getElementById("input_license_rate_edit").value;
        let request_data = {type:"edit_license", session:sessionid,name:name,rate:rate, id:id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_license").modal('toggle');
        show_licenses();
    }else{display_alert("User is not Admin!")};
}

function edit_pass(){
    if(admin){
        let id = document.querySelector(".selected_row.user").id;
        let pass = document.getElementById("input_pass_new_admin").value;
        let request_data = {type:"set_pass", session:sessionid,pass:pass, id:id};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            alert_no_success(data_obj);
        });
        $("#dialog_edit_user_admin").modal('toggle');
        show_user();
    }else{display_alert("User is not Admin!")};
}

function show_user(){
    if(admin){
        clear_output();
        let request_data = {type:"show_user",session:sessionid};
        post_server(request_data).then(function(response){
            let data = response;
            alert_no_login(data);
            alert_no_success(data);
            decodeURI(data);
            if(correct_query(data)){
                for(let i =0; i<data.length;i++){
                    let div_license=document.createElement("DIV");
                    div_license.id=data[i]["id"];
                    div_license.classList.add("user");
                    div_license.innerHTML=data[i]["username"] + ": " +data[i]["prename"] + " " + data[i]["name"];
                    output.appendChild(div_license);
                }
            }
        });
    }else{display_alert("User is not Admin!")};
}

function show_slots(){
    clear_output();
    if(admin){
        let request_data = {type:"show_slot",session:sessionid};
        post_server(request_data).then(function(response){
            let data = response;
            alert_no_login(data);
            alert_no_success(data);
            decodeURI(data);
            if(correct_query(data)){
                for(let i =0; i<data.length;i++){
                    let div_license=document.createElement("DIV");
                    div_license.id=data[i]["id"];
                    div_license.classList.add("slot");
                    if(data[i]["active"] == "1"){
                        div_license.innerHTML=data[i]["name"] + " (" + data[i]["begin"] + "-" + data[i]["end"]+ " ," + data[i]["duration"]+" min., " + data[i]["rate"]/100+ ") active";
                    }else{
                        div_license.innerHTML=data[i]["name"] + " (" + data[i]["begin"] + "-" + data[i]["end"]+ " ," + data[i]["duration"]+" min., " + data[i]["rate"]/100+ ")";
                    }
                    output.appendChild(div_license);
                }
            }
        });
    }else{display_alert("User is not Admin!")};
}

function show_licenses(){
    clear_output();
    if(admin){
        let request_data = {type:"show_licenses",session:sessionid};
        post_server(request_data).then(function(response){
            let data = response;
            alert_no_login(data);
            alert_no_success(data);
            decodeURI(data);
            if(correct_query(data)){
                for(let i =0; i<data.length;i++){
                    let div_license=document.createElement("DIV");
                    div_license.id=data[i]["id"];
                    div_license.classList.add("license");
                    div_license.innerHTML=data[i]["name"] + " (" + (data[i]["rate"]/100)+" Euro/Stunde)";
                    output.appendChild(div_license);
                }
            }
        });
    }else{display_alert("User is not Admin!")};
}

function show_groups(){
    clear_output();
    if(admin){
        let request_data = {type:"show_groups",session:sessionid};
        post_server(request_data).then(function(response){
            let data = response;
            alert_no_login(data);
            alert_no_success(data);
            decodeURI(data);
            if(correct_query(data)){
                for(let i =0; i<data.length;i++){
                    let div_license=document.createElement("DIV");
                    div_license.id=data[i]["id"];
                    div_license.classList.add("group");
                    div_license.innerHTML=data[i]["group_name"];
                    output.appendChild(div_license);
                }
            }
        });
    }else{display_alert("User is not Admin!")};
}

function delete_dsgvo(){
    let options = document.getElementById("input_choose_judoka").options;
    for(let i = 0; i<options.length; i++){
        if(options[i].selected){
            const request_data = {type:"delete_judoka", session:sessionid, id:options[i].value};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                alert_no_success(data_obj);
            }).then(function(){
                const request_data1 = {type:"delete_empty_entries", session:sessionid};
                post_server(request_data1).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    alert_no_success(data_obj);
                }); 
            });    
        }
    }
    $("#dialog_delete_dsgvo").modal('toggle');
}
