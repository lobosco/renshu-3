//https://gist.github.com/todofixthis/6f68c0e7f4378e6a1eebb81b978740df

function toggleModal(modal, state) {
  // Quick safety check, in case deferreds aren't working
  //  correctly.
  if(state == $('body').hasClass('modal-open')) {
    throw new Error(
      'Modal is already ' + (state ? 'shown' : 'hidden') + '!'
    );
  }
  
  var d = $.Deferred();
  
  modal
    .one(state ? 'shown.bs.modal' : 'hidden.bs.modal', d.resolve)
    .modal(state ? 'show' : 'hide');

  return d.promise();
};



function add_graduations(element){
    document.getElementById(element).innerHTML="<option value='9'>9. Kyu (weiß)</option>\n<option value='8'>8. Kyu (weiß-gelb)</option>\n<option value='7'>7. Kyu (gelb)</option>\n<option value='6'>6. Kyu (gelb-orange)</option>\n<option value='5'>5. Kyu (orange)</option>\n<option value='4'>4. Kyu (orange-grün)</option>\n<option value='3'>3. Kyu (grün)</option>\n<option value='2'>2. Kyu (blau)</option>\n<option value='1'>1. Kyu (braun)</option>\n<option value='-1'>1. Dan (schwarz)</option>\n<option value='-2'>2. Dan (schwarz)</option>\n<option value='-3'>3. Dan (schwarz)</option>\n<option value='-4'>4. Dan (schwarz)</option>\n<option value='-5'>5. Dan(schwarz)</option>\n<option value='-6'>6. Dan (rot-weiß)</option>\n<option value='-7'>7. Dan (rot-weiß)</option>\n<option value='-8'>8. Dan (rot-weiß)</option>";
}

function add_places(element){
    document.getElementById(element).innerHTML="<option value='-1'>teilg.</option>\n<option value='0'>-</option>\n<option value='1'>1. Platz</option>\n<option value='2'>2. Platz</option>\n<option value='3'>3. Platz</option>\n<option value='4'>4. Platz</option>\n<option value='5'>5. Platz</option>\n<option value='6'>6. Platz</option>\n<option value='7'>7. Platz</option>\n<option value='8'>8. Platz</option>\n<option value='9'>9. Platz</option>\n<option value='10'>10. Platz</option>";
}

//no
function open_dialog_add_judoka(){
    $('#dialog_loading').modal('show');
    add_graduations('input_grade');
    let request_data = {type:"get_qualification_all", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu = document.getElementById("input_license");
        let text = "";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["name"] + "</option>"; 
        }
        menu.innerHTML = text;
    }).then(function(){
        request_data = {type:"dummy", session:sessionid};
        post_server(request_data).then(function(response){
            $('#dialog_loading').modal('toggle');
        }).then(function(){
            $('#dialog_add_judoka').modal('show');
        });
    });
}

//yes
function open_dialog_add_training(){
    $('#dialog_loading').modal('show');
    let request_data = {type:"get_slots_active", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu = document.getElementById("input_slot");
        let text = "";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["name"] + "</option>"; 
        }
        menu.innerHTML = text;
    }).then(function(){
        request_data = {type:"get_trainer_all", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " +part["prename"] + " " + part["name"] + "</option>"; 
            }
            document.getElementById("input_trainer").innerHTML = text;
        }).then(function(){
            request_data = {type:"show_judoka_sort", session:sessionid};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                let text = "";
                for (let i=0; i<data_obj.length; i++){
                    let part = data_obj[i];
                    text += "<option value='" + part["id"] +"'> " +part["prename"] + " " + part["name"] + "</option>"; 
                }
                document.getElementById("input_pupils").innerHTML = text;
            }).then(function(){
                $('#dialog_loading').modal('toggle');
            }).then(function(){
                $('#dialog_add_training').modal('show');
            });
        });
    });
}

//yes
function open_dialog_edit_judoka(){
    $('#dialog_loading').modal('show');
    add_graduations('input_grade_edit');
    let request_data = {type:"get_qualification_all", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu = document.getElementById("input_license_edit");
        let text = "";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["name"] + "</option>"; 
        }
        menu.innerHTML = text;
    }).then(function(){;
        selected_div = document.querySelector('.selected_row.judoka');
        if(selected_div){
            request_data = {type:"judoka_get_info", session:sessionid,judoka_id:selected_div.id};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                document.getElementById("input_name_edit").value = data_obj[0]["name"];
                document.getElementById("input_prename_edit").value = data_obj[0]["prename"];
                document.getElementById("input_grade_edit").value = data_obj[0]["grade"];
                document.getElementById("input_contact_edit").value =  decodeURIComponent(data_obj[0]["contact"]);
                document.getElementById("input_pass_edit").value = data_obj[0]["pass"];
                document.getElementById("input_wkl_edit").value = data_obj[0]["wkl"];
                document.getElementById("input_comment_edit").value = data_obj[0]["comment"];
                document.getElementById("input_birthdate_edit").value = data_obj[0]["birth_date"];
                if(data_obj[0]["examiner"] == "1"){
                    document.getElementById("input_examiner_license_edit").checked = true;
                }
                if(data_obj[0]["dsgvo_photo"] == "1"){
                    document.getElementById("input_dsgvo_edit").checked = true;
                }
            }).then(function(){
                request_data = {type:"judoka_get_licenses", session:sessionid,judoka_id:selected_div.id};
                post_server(request_data).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    decodeURI(data_obj);
                    for(let i = 0; i<data_obj.length; i++){
                        for(let j=0;j<document.getElementById("input_license_edit").options.length;j++){
                            console.log(document.getElementById("input_license_edit").options[j].value); 
                            console.log(data_obj[i]["qualification_id"]);
                            if(document.getElementById("input_license_edit").options[j].value == data_obj[i]["qualification_id"]){
                                document.getElementById("input_license_edit").options[j].selected = true;
                            }
                        }
                    }
                }).then(function(){
                    $('#dialog_loading').modal('toggle');
                }).then(function(){
                    $('#dialog_edit_judoka').modal('show');
                });
            });
        }else{
            $('#dialog_loading').modal('toggle');
            $('#dialog_edit_judoka').modal('hide');
            display_alert("nothing selected");};
    });
}

//yes
function open_dialog_add_exam(){
    $('#dialog_loading').modal('show');
    let request_data = {type:"get_examiner_all", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu = document.getElementById("input_examiner");
        let text = "<option></option>";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
        }
        menu.innerHTML = text;
    }).then(function(){
        request_data = {type:"show_judoka_sort", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let text = "<option></option>";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
            }
            for(let i=0; i<20;i++){
                add_graduations('input_grade_received'+i);
                let menu = document.getElementById("input_examined"+i);
                menu.innerHTML = text;
            }
        }).then(function(){
            $('#dialog_loading').modal('toggle');
        }).then(function(){
            $('#dialog_add_exam').modal('show');
        });
    });
}

//yes
function open_dialog_edit_exam(){
    $('#dialog_loading').modal('show');
    let selected_div = document.querySelector('.selected_row.exam');
    if(selected_div){
        document.getElementById("input_date_exam_edit").value=document.getElementById("exam_date"+selected_div.id).innerHTML;
        document.getElementById("input_begin_exam_edit").value=document.getElementById("exam_begin"+selected_div.id).innerHTML;
        document.getElementById("input_end_exam_edit").value=document.getElementById("exam_end"+selected_div.id).innerHTML;
        document.getElementById("input_duration_exam_edit").value=document.getElementById("exam_duration"+selected_div.id).innerHTML;
        let request_data = {type:"get_examiner_all", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let menu = document.getElementById("input_examiner_edit");
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
            }
            menu.innerHTML = text;
        }).then(function(){
            request_data = {type:"exam_get_examiner", session:sessionid,id:selected_div.id};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                for(let i = 0; i<data_obj.length; i++){
                    for(let j=0;j<document.getElementById("input_examiner_edit").options.length;j++){
                        if(document.getElementById("input_examiner_edit").options[j].value == data_obj[i]   ["judoka_id_examiner"]){
                            document.getElementById("input_examiner_edit").options[j].selected = true;
                        }
                    }
                }
            }).then(function(){ 
                request_data = {type:"show_judoka_sort", session:sessionid};
                post_server(request_data).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    decodeURI(data_obj);
                    let text = "<option></option>";
                    for (let i=0; i<data_obj.length; i++){
                        let part = data_obj[i];
                        text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
                    }
                    for(let i=0; i<20;i++){
                        add_graduations('input_grade_received'+i+'_edit');
                        let menu = document.getElementById("input_examined"+i+"_edit");
                        menu.innerHTML = text;
                    }   
                }).then(function(){
                    request_data = {type:"show_exam_participants", session:sessionid,id:selected_div.id};
                    post_server(request_data).then(function(response){
                        let data_obj = response;
                        alert_no_login(data_obj);
                        decodeURI(data_obj);
                        let text = "";
                        for (let i=0; i<data_obj.length; i++){
                            let part = data_obj[i];
                            document.getElementById("input_examined"+i+"_edit").value = part["id"];
                            document.getElementById("input_grade_received"+i+"_edit").value = part["received_grade"];
                        }
                    }).then(function(){
                        $('#dialog_loading').modal('toggle');
                    }).then(function(){
                        $('#dialog_edit_exam').modal('show');
                    });
                });
            });
        });
    }else{
        $('#dialog_loading').modal('toggle');
        $('#dialog_edit_exam').modal('hide');
        display_alert("nothing selected");}
}


function open_dialog_edit_training(){
    $('#dialog_loading').modal('show');
    let selected_div = document.querySelector('.selected_row.training');
    if(selected_div){
        let request_data = {type:"get_slots_active", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let menu = document.getElementById("input_slot_edit");
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["name"] + "</option>"; 
            }
            menu.innerHTML = text;
        }).then(function(){
            request_data = {type:"training_get_info", session:sessionid,id:selected_div.id};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                document.getElementById("input_training_date_edit").value = data_obj[0]["date"];
                document.getElementById("input_slot_edit").value = data_obj[0]["slot_id"];
                document.getElementById("input_plan_edit").value = data_obj[0]["plan"];
                document.getElementById("input_comment_training_edit").value = data_obj[0]["comment"];
            }).then(function(){
                request_data = {type:"get_trainer_all", session:sessionid};
                post_server(request_data).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    decodeURI(data_obj);
                    let text = "";
                    for (let i=0; i<data_obj.length; i++){
                        let part = data_obj[i];
                        text += "<option value='" + part["id"] +"'> " +part["prename"] + " " + part["name"] + "</option>"; 
                    }
                    document.getElementById("input_trainer_edit").innerHTML = text;
                }).then(function(){
                    request_data = {type:"training_get_trainer", session:sessionid,id:selected_div.id};
                    post_server(request_data).then(function(response){
                        let data_obj = response;
                        alert_no_login(data_obj);
                        decodeURI(data_obj);
                        for(let i = 0; i<data_obj.length; i++){
                            for(let j=0;j<document.getElementById("input_trainer_edit").options.length;j++){
                                if(document.getElementById("input_trainer_edit").options[j].value == data_obj[i]["judoka_id_trainer"]){
                                    document.getElementById("input_trainer_edit").options[j].selected = true;
                                }
                            }
                        }
                    }).then(function(){
                        request_data = {type:"show_judoka_sort", session:sessionid};
                        post_server(request_data).then(function(response){
                            let data_obj = response;
                            alert_no_login(data_obj);
                            decodeURI(data_obj);
                            let text = "";
                            for (let i=0; i<data_obj.length; i++){
                                let part = data_obj[i];
                                text += "<option value='" + part["id"] +"'> " +part["prename"] + " " + part["name"] + "</option>"; 
                            }
                            document.getElementById("input_pupils_edit").innerHTML = text;
                        }).then(function(){
                            request_data = {type:"training_get_participants", session:sessionid,id:selected_div.id};
                            post_server(request_data).then(function(response){
                                let data_obj = response;
                                alert_no_login(data_obj);
                                decodeURI(data_obj);
                                for(let i = 0; i<data_obj.length; i++){
                                    for(let j=0;j<document.getElementById("input_pupils_edit").options.length;j++){
                                        if(document.getElementById("input_pupils_edit").options[j].value == data_obj[i]["judoka_id_participant"]){
                                            document.getElementById("input_pupils_edit").options[j].selected = true;
                                        }
                                    }
                                }
                            }).then(function(){
                                $('#dialog_loading').modal('hide');
                            }).then(function(){
                                $('#dialog_edit_training').modal('show');
                            });
                        });
                    });
                });
            });
        });
    }else{
        $('#dialog_loading').modal('hide');
        $('#dialog_edit_training').modal('hide');
        display_alert("nothing selected");}
}

//yes
function open_dialog_add_comp(){
    $('#dialog_loading').modal('show');
    let request_data = {type:"get_trainer_all", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu = document.getElementById("input_coach");
        let text = "";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
        }
        menu.innerHTML = text;
    }).then(function(){
        request_data = {type:"show_judoka_sort", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let text = "<option></option>";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
            }
            for(let i=0; i<10;i++){
                add_places('input_result'+i);
                let menu = document.getElementById("input_fighter"+i);
                menu.innerHTML = text;
            }
        }).then(function(){
            $('#dialog_loading').modal('hide');
        }).then(function(){
            $('#dialog_add_comp').modal('show');
        });
    });
}

//yes
function open_dialog_edit_comp(){
    $('#dialog_loading').modal('show');
    let selected_div = document.querySelector('.selected_row.comp');
    if(selected_div){
        let request_data = {type:"get_trainer_all", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let menu = document.getElementById("input_coach_edit");
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
            }
            menu.innerHTML = text;
        }).then(function(){
            let request_data = {type:"get_coach_comp", session:sessionid,id:selected_div.id};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                for(let i = 0; i<data_obj.length; i++){
                    for(let j=0;j<document.getElementById("input_coach_edit").options.length;j++){
                        if(document.getElementById("input_coach_edit").options[j].value == data_obj[i]["id"]){
                            document.getElementById("input_coach_edit").options[j].selected = true;
                        }
                    }
                }
            }).then(function(){
                request_data = {type:"show_judoka_sort", session:sessionid};
                post_server(request_data).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    decodeURI(data_obj);
                    let text = "<option></option>";
                    for (let i=0; i<data_obj.length; i++){
                        let part = data_obj[i];
                        text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "</option>"; 
                    }
                    for(let i=0; i<10;i++){
                        add_places('input_result'+i+"_edit");
                        let menu = document.getElementById("input_fighter"+i+"_edit");
                        menu.innerHTML = text;
                    }
                }).then(function(){
                    request_data = {type:"get_comp_participants", session:sessionid,id:selected_div.id};
                    post_server(request_data).then(function(response){
                        let data_obj = response;
                        alert_no_login(data_obj);
                        decodeURI(data_obj);
                        let text = "";
                        for (let i=0; i<data_obj.length; i++){
                            let part = data_obj[i];
                            for(let j=0;j<document.getElementById("input_fighter"+i+"_edit").options.length;j++){
                                if(document.getElementById("input_fighter"+i+"_edit").options[j].value == part["id"]){
                                    document.getElementById("input_fighter"+i+"_edit").options[j].selected = true;
                                }
                            }
                            document.getElementById("input_class"+i+"_edit").value = part["class"]; 
                            document.getElementById("input_result"+i+"_edit").value = part["result"];
                            document.getElementById("input_comment"+i+"_edit").value = part["comment"];
                        }
                    }).then(function(){
                        request_data = {type:"get_comp_info", session:sessionid,id:selected_div.id};                 
                        post_server(request_data).then(function(response){
                            let data_obj = response;
                            alert_no_login(data_obj);
                            decodeURI(data_obj);
                            document.getElementById("input_comp_name_edit").value = data_obj[0]["name"];
                            document.getElementById("input_comp_date_edit").value = data_obj[0]["date"];
                            document.getElementById("input_location_edit").value = data_obj[0]["location"];
                        }).then(function(){
                            $('#dialog_loading').modal('toggle');
                        }).then(function(){
                            $('#dialog_edit_comp').modal('show');
                        });
                    });
                });
            });
        });
    }else{
        $('#dialog_loading').modal('toggle');
        $('#dialog_edit_comp').modal('hide');
        display_alert("nothing selected");}
}

//no
function open_dialog_edit_user(){
    $('#dialog_loading').modal('show');
    let request_data = {type:"get_user_info", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        document.getElementById("output_user_name").value = data_obj[0]["username"];
    }).then(function(){
        request_data =  {type:"dummy", session:sessionid};
        post_server(request_data).then(function(response){
            $('#dialog_loading').modal('toggle');
        }).then(function(){
            $('#dialog_edit_user').modal('show');
        });
    });
}

//yes
function open_dialog_edit_user_admin(){
    $('#dialog_loading').modal('show');
    let marked = document.querySelector(".selected_row.user");
    if(admin){
        if(marked){
            let request_data = {type:"show_judoka_sort", session:sessionid};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                let menu = document.getElementById("input_assoc_judoka_edit");
                let text = "";
                for (let i=0; i<data_obj.length; i++){
                    let part = data_obj[i];
                    text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "(" + part["birth_date"] +")</option>"; 
                }
                menu.innerHTML = text;
            }).then(function(){
                let request_data = {type:"get_user_info_pro", session:sessionid,id:marked.id};
                post_server(request_data).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    decodeURI(data_obj);
                    document.getElementById("input_username_edit_admin").value = data_obj[0]["username"];
                    document.getElementById("input_assoc_judoka_edit").value = data_obj[0]["assoc_judoka_id"];
                }).then(function(){
                    request_data = {type:"show_groups", session:sessionid};
                    post_server(request_data).then(function(response){
                        let data_obj = response;
                        alert_no_login(data_obj);
                        decodeURI(data_obj);
                        let menu = document.getElementById("input_group_edit");
                        let text = "";
                        for (let i=0; i<data_obj.length; i++){
                            let part = data_obj[i];
                            text += "<option value='" + part["id"] +"'> " + part["group_name"] + "</option>"; 
                        }
                        menu.innerHTML = text;
                    }).then(function(){
                        let marked = document.querySelector(".selected_row.user");
                        let request_data = {type:"get_user_groups",session:sessionid,id:marked.id};
                        post_server(request_data).then(function(response){
                            let data_obj = response;
                            alert_no_login(data_obj);
                            decodeURI(data_obj);
                            for(let i = 0; i<data_obj.length; i++){
                                for(let j=0;j<document.getElementById("input_group_edit").options.length;j++){
                                    if(document.getElementById("input_group_edit").options[j].value == data_obj[i]["group_id"]){
                                        document.getElementById("input_group_edit").options[j].selected = true;
                                    }
                                }
                            }
                        }).then(function(){
                            $('#dialog_loading').modal('toggle');
                        }).then(function(){
                            $('#dialog_edit_user_admin').modal('show');
                        });
                    });
                });
            });
        }else{
            $('#dialog_loading').modal('toggle');
            $('#dialog_edit_user_admin').modal('hide');
            display_alert("nothing selected")};
    }else{
        output.style.display="none";
        display_alert("User is not Admin!");
    }
}

//yes
function open_dialog_add_user(){
    if(admin){
        $('#dialog_loading').modal('toggle');
        let request_data = {type:"show_judoka_sort", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let menu = document.getElementById("input_assoc_judoka");
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "(" + part["birth_date"] +")</option>"; 
            }
            menu.innerHTML = text;
        }).then(function(){
            request_data = {type:"show_groups", session:sessionid};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                let menu = document.getElementById("input_group");
                let text = "";
                for (let i=0; i<data_obj.length; i++){
                    let part = data_obj[i];
                    text += "<option value='" + part["id"] +"'> " + part["group_name"] + "</option>"; 
                }
                menu.innerHTML = text;
            }).then(function(){
                $('#dialog_loading').modal('toggle');
            }).then(function(){
                $('#dialog_add_user').modal('show');
            });
        });
    }else{
        output.style.display="none";
        display_alert("User is not Admin!");
    }  
}

// yes
function open_dialog_add_group(){
    if(admin){
        $('#dialog_loading').modal('show');
        let request_data = {type:"show_judoka_sort", session:sessionid};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let menu = document.getElementById("input_readable");
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "(" + part["birth_date"] +")</option>"; 
            }
            menu.innerHTML = text;
        }).then(function(){
            request_data = {type:"show_user", session:sessionid};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                let menu = document.getElementById("input_members");
                let text = "";
                for (let i=0; i<data_obj.length; i++){
                    let part = data_obj[i];
                    text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + " ("+part["username"] + ") </option>"; 
                }
                menu.innerHTML = text;
            }).then(function(){
                $('#dialog_loading').modal('toggle');
            }).then(function(){
                $('#dialog_add_group').modal('show');
            });
        });
    }else{
        output.style.display="none";
        display_alert("User is not Admin!");
    }
}

//yes
function open_dialog_edit_group(){
    if(admin){
        $('#dialog_loading').modal('show');
        let marked = document.querySelector(".selected_row.group");
        if(marked){
            let request_data = {type:"show_judoka_all", session:sessionid};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                decodeURI(data_obj);
                let menu = document.getElementById("input_readable_edit");
                let text = "";
                for (let i=0; i<data_obj.length; i++){
                    let part = data_obj[i];
                    text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + "(" + part["birth_date"] +")</option>"; 
                }
                menu.innerHTML = text;
            }).then(function(){
                let marked = document.querySelector(".selected_row.group");
                let request_data = {type:"get_group_info", session:sessionid,id:marked.id};
                post_server(request_data).then(function(response){
                    let data_obj = response;
                    alert_no_login(data_obj);
                    decodeURI(data_obj);
                    document.getElementById("input_groupname_edit").value = data_obj[0]["group_name"];
                }).then(function(){ 
                    let marked = document.querySelector(".selected_row.group");
                    let request_data = {type:"get_group_permissions",session:sessionid,id:marked.id};
                    post_server(request_data).then(function(response){
                        let data_obj = response;
                        alert_no_login(data_obj);
                        decodeURI(data_obj);
                        for(let i = 0; i<data_obj.length; i++){
                            for(let j=0;j<document.getElementById("input_readable_edit").options.length;j++){
                                if(document.getElementById("input_readable_edit").options[j].value == data_obj[i]["readable_judoka_id"]){
                                    document.getElementById("input_readable_edit").options[j].selected = true;
                                }
                            }
                        }
                    }).then(function(){
                        request_data = {type:"show_user", session:sessionid};
                        post_server(request_data).then(function(response){
                            let data_obj = response;
                            alert_no_login(data_obj);
                            decodeURI(data_obj);
                            let menu = document.getElementById("input_members_edit");
                            let text = "";
                            for (let i=0; i<data_obj.length; i++){
                                let part = data_obj[i];
                                text += "<option value='" + part["id"] +"'> " + part["prename"] + " " + part["name"] + " ("+part["username"] + ") </option>"; 
                            }
                            menu.innerHTML = text;
                        }).then(function(){
                            let marked = document.querySelector(".selected_row.group");
                            let request_data = {type:"get_group_members",session:sessionid,id:marked.id};
                            post_server(request_data).then(function(response){
                                let data_obj = response;
                                alert_no_login(data_obj);
                                decodeURI(data_obj);
                                for(let i = 0; i<data_obj.length; i++){
                                    for(let j=0;j<document.getElementById("input_members_edit").options.length;j++){
                                        if(document.getElementById("input_members_edit").options[j].value == data_obj[i]["user_id"]){
                                            document.getElementById("input_members_edit").options[j].selected = true;
                                        }
                                    }
                                }
                            }).then(function(){
                                $('#dialog_loading').modal('toggle');
                            }).then(function(){
                                $('#dialog_edit_group').modal('show');
                            });
                        });
                    });
                });
            });
        }else{
            $('#dialog_loading').modal('toggle');
            $('#dialog_edit_group').modal('hide');
            display_alert("nothing selected");}
    }else{
        output.style.display="none";
        display_alert("User is not Admin!");
    }
}

//nope
function open_dialog_edit_license(){
    if(admin){
        $('#dialog_loading').modal('show');
        let marked = document.querySelector(".selected_row.license");
        if(marked){
            let request_data = {type:"get_license_info",session:sessionid,id:marked.id};
            post_server(request_data).then(function(response){
                let data = response;
                alert_no_login(data);
                decodeURI(data);
                document.getElementById("input_license_name_edit").value=data[0]["name"];
                document.getElementById("input_license_rate_edit").value=data[0]["rate"];
            }).then(function(){
                request_data =  {type:"get_user_info", session:sessionid};
                post_server(request_data).then(function(response){
                    $('#dialog_loading').modal('toggle');
                }).then(function(){
                    $('#dialog_edit_license').modal('show');
                });
            });
        }else{
            $('#dialog_loading').modal('toggle');
            $('#dialog_edit_license').modal('hide');
            display_alert("nothing selected");};
    }else{
        output.style.display="none";
        display_alert("User is not Admin!");
    }
}

// doesn't work
function open_dialog_edit_slot(){
    if(admin){
        $('#dialog_loading').modal('show');
        let marked = document.querySelector(".selected_row.slot");
        if(marked){
            let request_data = {type:"get_slot_info",session:sessionid,id:marked.id};
            post_server(request_data).then(function(response){
                let data = response;
                alert_no_login(data);
                decodeURI(data);
                document.getElementById("input_slotname_edit").value=data[0]["name"];
                document.getElementById("input_begin_edit").value=data[0]["begin"];
                document.getElementById("input_end_edit").value=data[0]["end"];
                document.getElementById("input_duration_edit").value=data[0]["duration"];
                document.getElementById("input_rate_slot_edit").value=data[0]["rate"];
                if(data[0]["active"] == "1"){
                    document.getElementById("input_slot_active_edit").checked = true;
                }
            }).then(function(){
                request_data =  {type:"dummy", session:sessionid};
                post_server(request_data).then(function(response){
                    $('#dialog_loading').modal('toggle');
                }).then(function(){
                    $('#dialog_edit_slot').modal('show');
                });
            });
        }else{
            $('#dialog_loading').modal('toggle');
            $('#dialog_edit_slot').modal('hide');
            display_alert("nothing selected");};
    }else{
        output.style.display="none";
        display_alert("User is not Admin!");
    }
}

// doesn't work
function open_dialog_print_training(){
    $('#dialog_loading').modal('show');
    let request_data = {type:"get_slots_active", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu = document.getElementById("input_list_slot");
        let text = "";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["name"] + "</option>"; 
        }
        menu.innerHTML = text;
    }).then(function(){
        request_data =  {type:"dummy", session:sessionid};
        post_server(request_data).then(function(response){
            $('#dialog_loading').modal('toggle');
        }).then(function(){
            $('#dialog_print_training').modal('show');
        });
    });
}

// doesn't work
function open_dialog_print_salary(){
    $('#dialog_loading').modal('show');
    let request_data = {type:"get_slots_active", session:sessionid};
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let menu0 = document.getElementById("input_print_exam_slot");
        let menu1 = document.getElementById("input_print_comp_slot");
        let text = "";
        for (let i=0; i<data_obj.length; i++){
            let part = data_obj[i];
            text += "<option value='" + part["id"] +"'> " + part["name"] + "</option>"; 
        }
        menu0.innerHTML = text;
        menu1.innerHTML = text;
    }).then(function(){
        request_data =  {type:"dummy", session:sessionid};
        post_server(request_data).then(function(response){
            $('#dialog_loading').modal('toggle');
        }).then(function(){
            $('#dialog_print_salary').modal('show');
        });
    });
}

//doesnt work
function open_dialog_delete_dsgvo(){
    if(admin){
        currentModalId = '#dialog_delete_dsgvo';
        $('#dialog_loading').modal('show');
        let date_current = new Date();
        let date_delete_from=(parseInt(date_current.getFullYear()) -1) +"-" + (parseInt(date_current.getMonth())+1) +"-"+ date_current.getDate();
        let request_data = {type:"show_judoka_delete", session:sessionid,date:date_delete_from};
        post_server(request_data).then(function(response){
            let data_obj = response;
            alert_no_login(data_obj);
            decodeURI(data_obj);
            let menu = document.getElementById("input_choose_judoka");
            let text = "";
            for (let i=0; i<data_obj.length; i++){
                let part = data_obj[i];
                text += "<option value='" + part["id"] +"'> " + part["prename"] +" " + part["name"] + "(" + part["birth_date"] +")</option>"; 
            }
            menu.innerHTML = text;
        }).then(function(){
            request_data =  {type:"dummy", session:sessionid};
            post_server(request_data).then(function(response){
                $('#dialog_loading').modal('toggle');
            }).then(function(){
                $('#dialog_delete_dsgvo').modal('show');
            });
        });
    }else{output.style.display="none";
        display_alert("User is not Admin!")}
}
$('#dialog_add_slot').on('show.bs.modal', function(e){
    if(!admin){
        output.style.display="none";
        display_alert("User is not Admin!");
    }
});
$('#dialog_add_license').on('show.bs.modal', function(e){
    if(!admin){
        output.style.display="none";
        display_alert("User is not Admin!");
    }
});


//yes
function open_dialog_show_user_info(){
    let userinfo_modal = $('#dialog_show_userinfo')
    loading_modal.modal('show');
    let request_data = {type:"show_user_info", session:sessionid};
    post_server(request_data).then(function(response){
        console.log("user info response received");
        console.log($('body'));
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        let username = document.getElementById("output_user_name");
        let groups = document.getElementById("output_user_groups");
        let judokaname = document.getElementById("output_judoka_name");
        let part = data_obj[0];
        username.innerHTML = part["username"];
        groups.innerHTML = part["usergroups"];
        judokaname.innerHTML = part["name"];
    }).then(function(){ // doesn't work yet
        request_data = {type:"dummy", session:sessionid};
        post_server(request_data).then(function(response){
            $('#dialog_loading').modal('toggle');
        }).then(function(){
            $('#dialog_show_userinfo').modal('show');
        });
    });
}


$('#dialog_login').on('hidden.bs.modal', function(e){clear_dialog("dialog_login");});
$('#dialog_add_judoka').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_judoka");});
$('#dialog_add_training').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_training");});
$('#dialog_add_slot').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_slot");});
$('#dialog_add_exam').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_exam");});
$('#dialog_add_comp').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_comp");});
$('#dialog_add_license').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_license");});
$('#dialog_add_user').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_user");});
$('#dialog_add_group').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_group");});
$('#dialog_edit_judoka').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_judoka");});
$('#dialog_edit_training').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_training");});
$('#dialog_edit_slot').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_slot");});
$('#dialog_edit_exam').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_exam");});
$('#dialog_edit_comp').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_comp");});
$('#dialog_edit_license').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_license");});
$('#dialog_edit_user').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_user");});
$('#dialog_edit_group').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_group");});
$('#dialog_edit_user_admin').on('hidden.bs.modal', function(e){clear_dialog("dialog_edit_user_admin");});
$('#dialog_request').on('hidden.bs.modal', function(e){clear_dialog("dialog_request");});
$('#dialog_add_judoka_csv').on('hidden.bs.modal', function(e){clear_dialog("dialog_add_judoka_csv");});
$('#dialog_print_salary').on('hidden.bs.modal', function(e){clear_dialog("dialog_print_salary");});
$('#dialog_print_comp').on('hidden.bs.modal', function(e){clear_dialog("dialog_print_comp");});
$('#dialog_print_training').on('hidden.bs.modal', function(e){clear_dialog("dialog_print_training");});
$('#dialog_delete_dsgvo').on('hidden.bs.modal', function(e){clear_dialog("dialog_delete_dsgvo");});
$('#dialog_show_userinfo').on('hidden.bs.modal', function(e){clear_dialog("dialog_show_userinfo");});

function add_judoka_csv(){
    let file = document.getElementById("input_csv_judoka").files[0];
    let Reader = new FileReader();
    // files is a FileList of File objects. List some properties.
    Reader.readAsText(file);
    Reader.onload=function(e){
        let contents = e.target.result;
        var csvArray = $.csv.toArrays(contents);
        console.log(csvArray);
        for(let row in csvArray){
            let count = 0;
            let judoka_name, judoka_prename, judoka_grade, judoka_birthdate, judoka_pass, judoka_contact, judoka_wkl, judoka_dsgvo, judoka_comment, judoka_license, judoka_examiner= "";
            
            for(let item in csvArray[row]){
                if(count == 0){
                    judoka_name = csvArray[row][item];
                }else if(count == 1){
                    judoka_prename = csvArray[row][item];
                }else if(count == 2){
                    judoka_birthdate = csvArray[row][item];
                }else if(count == 3){
                    judoka_grade = csvArray[row][item];
                }else if(count == 4){
                    judoka_pass = csvArray[row][item];
                }else if(count == 5){
                    judoka_wkl = csvArray[row][item];
                }else if(count == 6){
                    judoka_contact = csvArray[row][item];
                }else if(count == 7){
                    judoka_examiner = csvArray[row][item];
                }else if(count == 8){
                    judoka_dsgvo = csvArray[row][item];
                }else if(count == 9){
                    judoka_licenses = csvArray[row][item];
                }else if(count == 10){
                    judoka_comment = csvArray[row][item];
                }
                count ++;
            }
            let request_data = {type:"add_judoka", session:sessionid,judoka_name:judoka_name, judoka_prename:judoka_prename, judoka_graduation:judoka_grade, judoka_birthdate:judoka_birthdate,judoka_contact:judoka_contact,judoka_wkl:judoka_wkl,judoka_pass:judoka_pass,judoka_examiner:judoka_examiner,judoka_dsgvo:judoka_dsgvo,judoka_comment:judoka_comment,judoka_license:judoka_licenses};
            post_server(request_data).then(function(response){
                let data_obj = response;
                alert_no_login(data_obj);
                alert_no_success(data_obj);
            });    
        }
    }
    close_dialog("dialog_add_judoka_csv");
    show_judoka();
}

function load_csv(evt){
    let file = evt.target.files[0]; // FileList object first entry only
    let Reader = new FileReader();
    // files is a FileList of File objects. List some properties.
    Reader.readAsText(file);
    Reader.onload=function(e){
        let contents = e.target.result;
        let output = document.getElementById("csv_output");
        var csvArray = $.csv.toArrays(contents);
        console.log(csvArray);
        let html = "<div class='append'>+</div><table> <tr><td><b>Name</b></td><td><b>Vorname</b></td><td><b>Geburtsdatum</b></td> <td><b>Graduierung</b></td> <td class='clipped'><b>Passnummer</b></td> <td class='clipped'><b>Wettkampflizenz</b></td>  <td class='clipped'><b>Kontakt</b></td> <td class='clipped'><b>Prüfer</b></td> <td class='clipped'><b>Photo</b></td> <td class='clipped'> <b> Lizenzen </b> </td> <td class='clipped'> <b> Kommentar </b> </td>";
        for(let row in csvArray){
            let count = 0;
            html += '<tr>';
            for(let item in csvArray[row]){
                if(count > 3){
                    html += "<td class='clipped'>" + csvArray[row][item] + '</td>';
                }else{
                    html += '<td>' + csvArray[row][item] + '</td>';
                }
                count ++;
            }
            html += '</tr>'
        }
        html += '</table>'
        output.innerHTML = html;
    }  
}


document.getElementById('input_csv_judoka').addEventListener('change', load_csv, false);
