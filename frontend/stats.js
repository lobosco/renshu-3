function grades_to_array(gradearray){
    let gradename = {"-7":"7. Dan","-6":"6. Dan","-5":"5. Dan","-4":"4. Dan","-3":"3. Dan","-2":"2. Dan","-1":"1. Dan","1":"1. Kyu","2":"2. Kyu","3":"3. Kyu","4":"4. Kyu","5":"5. Kyu","6":"6. Kyu","7":"7. Kyu","8":"8. Kyu","9":"ohne Gurt"} 
    let numbers = gradearray.split(",");
    let result = [];
    for( let i = 0; i< numbers.length; i++){
        let gradenum = numbers[i].split(":");
        result.push({name: gradename[gradenum[0]], count:gradenum[1], num:gradenum[0]});
    }
    return result;
}

function data_to_results(data){
    let result = [];
    for(let i=0; i<data.length;i++){
        if(data[i]["result"] == "-1"){
            result.push({name: "teilg.", count:data[i]["nr"], num:data[i]["result"]});
        }else{
            result.push({name: data[i]["result"]+".Platz", count:data[i]["nr"], num:data[i]["result"]});
        }
    }
    return result;
}

function data_to_licenses(data){
    let result = [];
    for(let i=0; i<data.length;i++){
        result.push({name: data[i]["name"], count:data[i]["nr"], num:data[i]["id"]});
    }
    return result;
}

function create_pie_chart(dataset, destination,window){
    let width= 700;
    let height = 300;
    let radius = (height-110)/2;
    let pie = d3.pie().value(function(d){return d.count}).sort(null);
    let arc=d3.arc().innerRadius(0).outerRadius(radius);
    let pos = d3.arc().innerRadius(0).outerRadius(2*radius+90);
    let div = d3.select(window.document.getElementById(destination));
    let svg_pie = div.append("svg").attr("width",width).attr("height",height);
    let g = svg_pie.append("g").attr("transform", "translate("+width/2+","+height/2+")");
    let path=g.selectAll('path') .data(pie(dataset)).enter().append('path').attr('d',arc).attr('fill',function(d,i){return "hsl("+(d.data.num*40)+",100%,75%)";}).attr("style","stroke:black;stroke-width:1px");
    let text=g.selectAll('text').data(pie(dataset)).enter().append("text").attr("transform", function (d) {return "translate(" + pos.centroid(d) + ")";}).attr("text-anchor", "middle").text(function(d){return d.data.name+" ("+d.data.count+")" ;});
}

function create_pie_ages(dataset, destination,window){
    let width= 700;
    let height = 300;
    let radius = (height-90)/2;
    let colorset=d3.scaleOrdinal().range(['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#ffff33']);
    let pie = d3.pie().value(function(d){return d.count}).sort(null);
    let arc=d3.arc().innerRadius(0).outerRadius(radius);
    let pos = d3.arc().innerRadius(0).outerRadius(2*radius+70);
    let div = d3.select(window.document.getElementById(destination));
    let svg_pie = div.append("svg").attr("width",width).attr("height",height);
    let g = svg_pie.append("g").attr("transform", "translate("+width/2+","+height/2+")");
    let path=g.selectAll('path') .data(pie(dataset)).enter().append('path').attr('d',arc).attr('fill',function(d,i){return colorset(i);}).attr("style","stroke:black;stroke-width:1px");
    let text=g.selectAll('text').data(pie(dataset)).enter().append("text").attr("transform", function (d) {return "translate(" + pos.centroid(d) + ")";}).attr("text-anchor", "middle").text(function(d){return d.data.name+" ("+d.data.count+")" ;});
}

function create_pie_grades(dataset, destination,window){
    let width= 700;
    let height = 400;
    let radius = (height-90)/2;
    let colorset=d3.scaleOrdinal().domain(["7. Dan","6. Dan","5. Dan","4. Dan","3. Dan","2. Dan","1. Dan","1. Kyu","2. Kyu","3. Kyu","4. Kyu","5. Kyu","6. Kyu","7. Kyu","8. Kyu","ohne Gurt"]).range(['#000000','#000000','#000000','#000000','#000000','#000000','#000000','#996633','#0000FF','#00FF00','url(#gradient_og)','#FF8000','url(#gradient_go)','#ffff00','url(#gradient_wg)','#ffffff']);
    let pie = d3.pie().value(function(d){return d.count});
    let arc=d3.arc().innerRadius(0).outerRadius(radius);
    let pos = d3.arc().innerRadius(0).outerRadius(2*radius+70);
    let div = d3.select(window.document.getElementById(destination));
    let svg_pie = div.append("svg").attr("width",width).attr("height",height);
    let defs = svg_pie.append("defs");
    let gradient_wg = defs.append("linearGradient").attr("id","gradient_wg");
    let gradient_wg_g = gradient_wg.append("stop").attr("offset","50%").attr("stop-color","#ffff00");
    let gradient_wg_w = gradient_wg.append("stop").attr("offset","0%").attr("stop-color","#ffffff");
    let gradient_go = defs.append("linearGradient").attr("id","gradient_go");
    let gradient_go_g = gradient_go.append("stop").attr("offset","50%").attr("stop-color","#ffff00");
    let gradient_go_o = gradient_go.append("stop").attr("offset","0%").attr("stop-color","#ff8000");
    let gradient_og = defs.append("linearGradient").attr("id","gradient_og");
    let gradient_og_g = gradient_og.append("stop").attr("offset","50%").attr("stop-color","#00ff00");
    let gradient_og_o = gradient_og.append("stop").attr("offset","0%").attr("stop-color","#ff8000");
    let g = svg_pie.append("g").attr("transform", "translate("+width/2+","+height/2+")");
    let path=g.selectAll('path') .data(pie(dataset)).enter().append('path').attr('d',arc).attr('fill',function(d,i){return colorset(d.data.name);}).attr("style", function(d,i){return "stroke:black;stroke-width:1px;";});
    let text=g.selectAll('text').data(pie(dataset)).enter().append("text").attr("transform", function (d) {return "translate(" + pos.centroid(d) + ")";}).attr("text-anchor", "middle").text(function(d){return d.data.name+" ("+d.data.count+")" ;});
}

function create_stat_layout(window){
    window.name="Statistics";
    let today = new Date();
    let day_current = today.getDate();
    let month_current = today.getMonth()+1;
    let year_current = today.getFullYear();
    window.document.write("<head> <style> body{font-size:10pt;}</style> <meta charset='UTF-8'>  </head><body> <h1> Statistiken des Jahres " + year_current +"</h1> <p> Stand: "+day_current+"." + month_current+ "." +year_current+ "</p> <h2>Altersklassenübersicht</h2> <h3>Judoka nach Altersklassen</h3><div id='judoka_ages'></div> <h3>Wettkämpfer nach Altersklassen</h3><div id='competitors_ages'></div> <h3>Graduierungen nach Altersklassen</h3><h4> Altersklasse U10</h4><div id='grades_u10'></div> <h4> Altersklasse U12</h4><div id='grades_u12'></div><h4> Altersklasse U15</h4> <div id='grades_u15'></div> <h4> Altersklasse U18</h4><div id='grades_u18'></div> <h4> Altersklasse U21</h4><div id='grades_u21'></div><h4> Altersklasse Aktive</h4> <div id='grades_aktive'></div> <h2>Trainingsstatistik</h2> <h3>Durchschnittliche Trainingsteilnahme</h3><div id='trainings'></div> <h3>Trainer</h3> <p>Anzahl registrierter Judoka mit Lizenz oder Traineramt ohne Lizenz: <span id='trainer_num'><span></p> <h4>Lizenzen</h4><div id='licenses'></div><h2>Prüfungen</h2><h3>Prüfer</h3> <p>Anzahl registrierter Judoka mit Prüferlizenz: <span id='examiner_num'><span></p>  <h4>abgelegte Prüfungen</h4><div id='exam_grades'></div> <h2>Wettkampferfolge</h2><div id='comp_res'></div></body>");
}

function show_stats(){
    let width = 700;
    let height = 300;
    let stat_window = window.open();
    create_stat_layout(stat_window);
    let request_data = {type:"get_judoka_age", session: sessionid};
    post_server(request_data).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            let judoka_u10 = data[0]["U10"];
            let judoka_u12 = data[0]["U12"];
            let judoka_u15 = data[0]["U15"];
            let judoka_u18 = data[0]["U18"];
            let judoka_u21 = data[0]["U21"];
            let judoka_aktive = data[0]["Aktive"];
            let dataset = [{name:"U10", count:judoka_u10},{name:"U12", count:judoka_u12},{name:"U15", count:judoka_u15},{name:"U18", count:judoka_u18},{name:"U21", count:judoka_u21},{name:"Aktive", count:judoka_aktive}];
            create_pie_ages(dataset, "judoka_ages",stat_window);
        }
    });
    let request_comp = {type:"get_competitors_age", session: sessionid};
    post_server(request_comp).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            let comp_u10 = data[0]["U10"];
            let comp_u12 = data[0]["U12"];
            let comp_u15 = data[0]["U15"];
            let comp_u18 = data[0]["U18"];
            let comp_u21 = data[0]["U21"];
            let comp_aktive = data[0]["Aktive"];
            let dataset = [{name:"U10", count:comp_u10},{name:"U12", count:comp_u12},{name:"U15", count:comp_u15},{name:"U18", count:comp_u18},{name:"U21", count:comp_u21},{name:"Aktive", count:comp_aktive}];
            create_pie_ages(dataset, "competitors_ages",stat_window);
        }
    });
    let request_slots = {type:"get_average_slot", session:sessionid};
    post_server(request_slots).then(function(response){
        let data=response;
        let averages = [];
        let slots = [];
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){ //get slot names etc.
            for(let i = 0; i<data.length; i++){
                averages.push(data[i]["average"]);
                slots.push(data[i]["id"]);
            }
            let div_chart0 = d3.select(stat_window.document.getElementById("trainings"))
            let svg = div_chart0.append("svg");    
            let x = d3.scaleBand().range([0,width]).domain(slots.map(function(d,i){return d;}));
            let y = d3.scaleLinear().range([height,0]).domain([0, Math.max.apply(Math,averages)]);
            let xAxis = d3.axisBottom(x);
            let chart = svg.attr("width", width).attr("height", height+100);
            let bar = chart.selectAll("g").data(averages).enter().append("g").attr("transform", function(d, i) { return "translate(" + i * x.bandwidth() + ", 0)"; });
            bar.append("rect").attr("y", function(d){return y(d)+50}).attr("height", function(d){return height-y(d);}).attr("width", x.bandwidth()).attr('fill', function(d,i){return "rgb(0,0," + d*20 + ")";});
            bar.append("text").attr("y", function(d) { return y(d)+30; }).attr("x",x.bandwidth()/4).text(function(d) { return d;}).attr("width", x.bandwidth());
            height_total = height + 70;
            chart.append("g").attr("class", "xaxis").attr("transform", "translate(0," + height_total + ")").call(xAxis);
        }
    });
    let request_grades = {type:"get_grades_age", session: sessionid};
    post_server(request_grades).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            let grades_u10 = grades_to_array(data[0]["U10"]);
            let grades_u12 = grades_to_array(data[0]["U12"]);
            let grades_u15 = grades_to_array(data[0]["U15"]);
            let grades_u18 = grades_to_array(data[0]["U18"]);
            let grades_u21 = grades_to_array(data[0]["U21"]);
            let grades_aktive = grades_to_array(data[0]["Aktive"]);
            create_pie_grades(grades_u10, "grades_u10",stat_window);
            create_pie_grades(grades_u12, "grades_u12",stat_window);
            create_pie_grades(grades_u15, "grades_u15",stat_window);
            create_pie_grades(grades_u18, "grades_u18",stat_window);
            create_pie_grades(grades_u21, "grades_u21",stat_window);
            create_pie_grades(grades_aktive, "grades_aktive",stat_window);
        }
    });
    let request_trainer = {type:"get_trainer_all", session: sessionid};
    post_server(request_trainer).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            stat_window.document.getElementById("trainer_num").innerHTML = data.length;
        }
    });
    let request_examiner = {type:"get_examiner_all", session: sessionid};
    post_server(request_examiner).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            stat_window.document.getElementById("examiner_num").innerHTML = data.length;
        }
    });
    let request_lic = {type:"get_count_licenses", session: sessionid};
    post_server(request_lic).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            let array_lic = data_to_licenses(data);
            create_pie_chart(array_lic,"licenses",stat_window);
        }
    });
    let request_exam = {type:"get_exam_grades", session: sessionid};
    post_server(request_exam).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            let array_exam = grades_to_array(data[0]["cnt"]);
            create_pie_grades(array_exam,"exam_grades",stat_window);
        }
    });
    let request_comp_res = {type:"get_competition_results", session: sessionid};
    post_server(request_comp_res).then(function(response){
        let data=response;
        alert_no_login(data);
        decodeURI(data);
        if(correct_query(data)){
            console.log(data);
            let array_comp = data_to_results(data);
            create_pie_chart(array_comp,"comp_res",stat_window);
        }
    });
}