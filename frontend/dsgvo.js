// Request of all saved data according to gdpr: 

// user choosen, then 

function set_dsgvo_template(window){
    window.name = "Datenübersicht DSGVO";
    let today = new Date();
    let day_current = today.getDate();
    let month_current = today.getMonth()+1;
    let year_current = today.getFullYear();
    window.document.write(" <style> body{font-size:10pt;}</style> <meta charset='UTF-8'>  </head><body> <h1 id='name_header'> </h1> <p> Stand: "+day_current+"." + month_current+ "." +year_current+ "</p> <h2>Judokadaten</h2><div id='data'> </div> <h2>Trainingsteilnahme (Trainer)</h2><div id='training_trainer'></div> <h2>Trainingsteilnahme (Schüler)</h2><div id='training_participant'></div> <h2>Prüfungsteilnahmen (Prüfer)</h2><div id='exam_examiner'></div> <h2>Prüfungsteilnahme (Prüfling)</h2><div id='exam_participant'></div><h2>Wettkampfteilnahme (Betreuer)</h2> <div id='comp_coach'></div> <h2> Wettkampfteilnahmen (TN)</h2><div id='comp_participant'></div></body>");
}

function print_dsgvo(){
    let judoka_id = document.querySelector('.selected_row.judoka').id;
    let dsgvo_window = window.open();
    set_dsgvo_template(dsgvo_window);
    let request_data = {type:"judoka_get_info", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data = response;
        decodeURI(data);
        alert_no_login(data);
        if(correct_query(data)){
            let part = data[0]
            dsgvo_window.document.getElementById('name_header').innerHTML = "Auskunft nach Art. 15 DSGVO über " + part["prename"] + " " + part["name"];
            dsgvo_window.document.getElementById('data').innerHTML = "<p> <b>Vorname: </b>" +  part["prename"] + "</p><p> <b>Nachname: </b>" +  part["name"] + "</p><p> <b>Geburtsdatum: </b>" +  part["birth_date"] + "</p><p> <b>Graduierung: </b>" +  part["grade"] + "</p><p> <b>Passnummer: </b>" +  part["pass"] + "</p><p> <b>Nummer Wettkampflizenz: </b>" +  part["wkl"] + "</p><p> <b>Zustimmung Photos: </b>" +  part["dsgvo_photo"] + "</p><p> <b>Prüferlizenz: </b>" +  part["examiner"] + "</p><p> <b>Lizenzen: </b>" +  part["license"] + "</p><p> <b>Kontakt: </b>" +  part["contact"] + "</p><p> <b>Kommentare: </b>" +  part["comment"]+ "</p>";
        }
    });
    request_data = {type:"judoka_get_training", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data = response;
        decodeURI(data);
        alert_no_login(data);
        if(correct_query(data)){
            let text="";
            for(let i = 0; i<data.length; i++){
                let part = data[i];
                let id = part["trainingid"];
                let slot_id = part["slotid"];
                let name = part["name"];
                let date=part["date"];
                let slot_div = dsgvo_window.document.getElementById("slot"+slot_id);
                if(!slot_div){
                    slot_div=dsgvo_window.document.createElement("DIV");
                    slot_div.id="slot"+slot_id;
                    slot_div.innerHTML="<h3>"+name+"</h3>";
                    dsgvo_window.document.getElementById('training_participant').appendChild(slot_div);
                }
                let training_div = dsgvo_window.document.createElement("DIV");
                training_div.id="training"+id;
                training_div.classList.add("indent","training");
                training_div.innerHTML=date;
                slot_div.appendChild(training_div);
            }
        }
    });
    request_data = {type:"judoka_get_comp", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "";
            for(let i = 0; i<data_obj.length; i++){
                let part=data_obj[i];
                console.log(part);
                text += "<div id='" + part["id"] + "'> "+ part["name"] + " (" + part["location"] + "," + part["date"] + "):" + part["class"] + " " + part["result"] +" (" + part["comment"] + ")</div>"; 
            }
            dsgvo_window.document.getElementById('comp_participant').innerHTML = text;
        }
    });
    request_data = {type:"judoka_get_exam", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "";
            for(let i = 0; i<data_obj.length; i++){
                let part=data_obj[i];
                if(part["received_grade"] <0){
                    text += "<div id='" + part["id"] + "'> "+ part["date"] + " (" + Math.abs(part["received_grade"]) +". Dan ) ["+part["examiner"]+"]</div>";
                }else{
                    text += "<div id='" + part["id"] + "'> "+ part["date"] + " (" + part["received_grade"] +". Kyu ) ["+part["examiner"]+"]</div>";
                }
            }
            dsgvo_window.document.getElementById("exam_participant").innerHTML = text;
        }
    });
    request_data = {type:"judoka_get_trainer", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data = response;
        decodeURI(data);
        alert_no_login(data);
        if(correct_query(data)){
            let text="";
            for(let i = 0; i<data.length; i++){
                let part = data[i];
                let id = part["trainingid"];
                let slot_id = part["slotid"];
                let name = part["name"];
                let date=part["date"];
                let slot_div = dsgvo_window.document.getElementById("trainer_slot"+slot_id);
                if(!slot_div){
                    slot_div=dsgvo_window.document.createElement("DIV");
                    slot_div.id="trainer_slot"+slot_id;
                    slot_div.innerHTML="<h3>"+name+"</h3>";
                    dsgvo_window.document.getElementById('training_trainer').appendChild(slot_div);
                }
                let training_div = dsgvo_window.document.createElement("DIV");
                training_div.id="training"+id;
                training_div.classList.add("indent","training");
                training_div.innerHTML=date;
                slot_div.appendChild(training_div);
            }
        }
    });
    request_data = {type:"judoka_get_examiner", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "";
            for(let i = 0; i<data_obj.length; i++){
                let part=data_obj[i];
                text += "<div id='" + part["id"] + "'> "+ part["date"] +"</div>";
            }
            dsgvo_window.document.getElementById("exam_examiner").innerHTML = text;
        }
    });   
    request_data = {type:"judoka_get_coach", session:sessionid,judoka_id:judoka_id}
    post_server(request_data).then(function(response){
        let data_obj = response;
        alert_no_login(data_obj);
        decodeURI(data_obj);
        if(correct_query(data_obj)){
            let text = "";
            for(let i = 0; i<data_obj.length; i++){
                let part=data_obj[i];
                text += "<div id='" + part["id"] + "'> "+ part["name"] + " (" + part["location"] + "," + part["date"] + ")</div>"; 
            }
            dsgvo_window.document.getElementById('comp_coach').innerHTML = text;
        }
    });
}