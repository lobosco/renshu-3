#ifndef _UTILITY_H_
#define _UTILITY_H_

// global Header
#include "common.h"

class  Utility {
    std::string dbAdress;
    std::string dbUser;
    std::string dbPass;
    std::string dbName;
    public:
       void initDB(std::string dbAdress, std::string dbUser, std::string dbPass, std::string dbName);
       boost::beast::http::response<boost::beast::http::string_body> createResponse(std::string &statement, unsigned version); 
    private:
       std::string executeSQL(std::string type,std::vector<std::string> arguments);
};
#endif
