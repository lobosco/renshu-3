#include "common.h"

using namespace std;

boost::beast::http::status set_status(std::string result){
    if(result == "0"){
        return boost::beast::http::status::internal_server_error;
    }else{
        return boost::beast::http::status::ok;
    }
}

string urlEncode(const string &value) {
    ostringstream escaped;
    escaped.fill('0');
    escaped << hex;
    for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
        string::value_type c = (*i);
        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }
        // Any other characters are percent-encoded
        escaped << uppercase;
        escaped << '%' << setw(2) << int((unsigned char) c);
        escaped << nouppercase;
    }
    return escaped.str();
}

unsigned char hexToChar(const std::string &str) {
    short c = 0;
    if(!str.empty()) {
        std::istringstream in(str);
        in >> std::hex >> c;
        if(in.fail()) {
            throw std::runtime_error("stream decode failure");
        }
    }
    return static_cast<unsigned char>(c);
}

std::string urlDecode(const std::string &toDecode){
    std::ostringstream out;
    for(std::string::size_type i=0; i < toDecode.length(); ++i) {
        if(toDecode.at(i) == '%') {
            std::string str(toDecode.substr(i+1, 2));
        out << hexToChar(str);
        i += 2;
        } else {
            out << toDecode.at(i);
        }
    }
    return out.str();
}

std::vector<std::string> splitString (std::string input, char delimiter){
    std::vector<std::string> arguments;
    std::stringstream f(input);
    std::string s;
    while (std::getline(f, s, delimiter)) {
        arguments.push_back(s);
    }
    return arguments;
};

std::string clearFormatting(std::string string){
    std::string string2 = urlDecode(string);
    std::replace(string2.begin(),string2.end(),'+',' ');
    return string2;
}

void Utility::initDB(std::string inputAdress, std::string inputName, std::string inputUser, std::string inputPass){
    dbAdress = inputAdress;
    dbName = inputName;
    dbUser = inputUser;
    dbPass = inputPass;
};

std::string Utility::executeSQL(std::string type, std::vector<std::string> arguments){
    try{
        sql::Driver *driver;
        sql::Connection *con;
        sql::PreparedStatement *stmt;
        sql::ResultSet* res;
        sql::ResultSetMetaData *res_meta;
        bool has_arguments = false;
        bool returns_single = false;
        bool returns_result = false;
        bool returns_id = false;
        bool log_result = false;
        std::string result = "";
        /* Create a connection */
        driver = get_driver_instance();
        driver -> threadInit();
        con = driver->connect(dbAdress, dbUser, dbPass);
        /* Connect to the MySQL test database */
        con->setSchema(dbName);
        if(type=="login"){
            stmt = con->prepareStatement("select id from sessions where user_id=(select id from user where username=? and pwd_hash=AES_ENCRYPT(?, UNHEX(SHA2(?,512))));");
            returns_single = true;
            has_arguments = true;
            log_result = true;
        }else if(type=="check_session"){
            stmt = con->prepareStatement("select user_id from sessions where timer>(CURRENT_TIME()) and count>0 and id=?;");
            has_arguments = true;
            returns_single = true;
        }else if(type=="update_session"){
            stmt=con->prepareStatement("update sessions set count = count - 1 where id=?;");
            has_arguments = true;
        }else if(type=="delete_session"){
            stmt = con->prepareStatement("delete from sessions where id=?;");
            has_arguments = true;
        }else if(type=="select_userid"){
            stmt = con->prepareStatement("select id from user where username=? and pwd_hash=AES_ENCRYPT(?, UNHEX(SHA2(?,512)));");
            returns_single = true;
            has_arguments = true;
        }else if(type=="new_session"){
            stmt = con->prepareStatement("insert into sessions (id, user_id, count, timer) values (?, ?, \"525\", ADDTIME(CURRENT_TIME(), 001500));");
            has_arguments = true;
        }else if(type=="check_admin"){
            stmt = con->prepareStatement("select min(group_id) from groups_user inner join sessions on sessions.user_id=groups_user.user_id where sessions.id =?;");
            returns_single = true;
            has_arguments = true;
        }else if(type=="logout"){
            stmt = con->prepareStatement("delete from sessions where id=?;");
            has_arguments = true;
            log_result = true;
        }else if(type=="show_judoka"){
            stmt = con->prepareStatement("select judoka.id, judoka.name, judoka.prename,judoka.grade, judoka.pass, judoka.wkl, judoka.contact, judoka.birth_date, judoka.dsgvo_photo, judoka.examiner, judoka.comment, group_concat(qualification.name separator ';') as license from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id left join qualification_judoka on qualification_judoka.judoka_id=judoka.id left join qualification on qualification.id=qualification_judoka.qualification_id where groups_user.user_id =? group by judoka.id;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_judoka_sort"){
            stmt = con->prepareStatement("select judoka.id, judoka.name, judoka.prename,judoka.grade, judoka.pass, judoka.wkl, judoka.contact, judoka.birth_date, judoka.dsgvo_photo, judoka.examiner, judoka.comment, group_concat(qualification.name separator ';') as license from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id left join qualification_judoka on qualification_judoka.judoka_id=judoka.id left join qualification on qualification.id=qualification_judoka.qualification_id where groups_user.user_id =? group by judoka.id order by judoka.prename asc, judoka.name asc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_judoka_all"){
            stmt = con->prepareStatement("select judoka.id, judoka.name, judoka.prename,judoka.grade, judoka.pass, judoka.wkl, judoka.contact, judoka.birth_date, judoka.dsgvo_photo, judoka.examiner, judoka.comment from judoka order by judoka.prename asc, judoka.name asc;");
            returns_result = true;
        }else if(type=="check_judoka_duplicate"){
            stmt = con->prepareStatement("select judoka.id from judoka where name=? and prename=? and birth_date=?;");
            has_arguments = true;
            returns_single = true;
        }else if(type=="add_judoka"){
            stmt = con->prepareStatement("insert into judoka (name,prename,grade,pass,wkl,contact,birth_date,dsgvo_photo,examiner,comment) values (?,?,?,?,?,?,?,?,?,?);");
            has_arguments = true;
        }else if(type=="add_qualifications_judoka"){
            stmt = con->prepareStatement("insert into qualification_judoka (qualification_id, judoka_id) values (?, (select id from judoka where prename=? and name=?));");
            has_arguments = true;
        }else if(type=="add_permissions_judoka"){
            stmt = con->prepareStatement("insert into permissions (group_id, readable_judoka_id) values ('1', (select id from judoka where prename=? and name=?));");
            has_arguments = true;
        }else if(type=="get_qualification_all"){
            stmt = con->prepareStatement("select * from qualification;");
            returns_result = true;
        }else if(type=="get_slots_all"){
            stmt = con->prepareStatement("select * from slots;");
            returns_result = true;
        }else if(type=="get_slots_active"){
            stmt = con->prepareStatement("select * from slots where active='1';");
            returns_result = true;
        }else if(type=="get_trainer_all"){
            stmt = con->prepareStatement("select judoka.id,judoka.prename,judoka.name from judoka inner join qualification_judoka on qualification_judoka.judoka_id=judoka.id where qualification_judoka.qualification_id > 1 group by judoka.id order by judoka.prename asc;");
            returns_result = true;
        }else if(type=="judoka_get_comp"){
            stmt = con->prepareStatement("select competition.id, competition.name, competition.location, competition.date, competition_participants.class, competition_participants.result, competition_participants.comment from competition_participants inner join competition on competition_participants.competition_id = competition.id where competition_participants.judoka_id_participant=? order by competition.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_coach"){
            stmt = con->prepareStatement("select competition.id, competition.name, competition.location, competition.date from competition inner join competition_coachs on competition.id=competition_coachs.competition_id where competition_coachs.judoka_id_coach=? order by competition.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_training"){
            stmt = con->prepareStatement("select slots.id as slotid,slots.name,training.id as trainingid,training.date,training.plan,training.comment, group_concat(distinct judoka.prename,' ',judoka.name separator ',') as trainer from training_participants inner join training on training.id=training_participants.training_id  inner join slots on slots.id=training.slot_id inner join training_trainer on training_trainer.training_id=training.id inner join judoka on training_trainer.judoka_id_trainer=judoka.id where training_participants.judoka_id_participant =? group by training.id order by training.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_trainer"){
            stmt = con->prepareStatement("select slots.id as slotid,slots.name,training.id as trainingid,training.date from training_trainer inner join training on training.id=training_trainer.training_id  inner join slots on slots.id=training.slot_id where training_trainer.judoka_id_trainer =? group by training.id order by training.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_exam"){
            stmt = con->prepareStatement("select exam.id, exam.date, exam_participants.received_grade, group_concat(distinct judoka.prename,' ',judoka.name separator ',') as examiner from exam inner join exam_participants on exam.id=exam_participants.exam_id inner join exam_examiners on exam_examiners.exam_id = exam.id inner join judoka on exam_examiners.judoka_id_examiner=judoka.id where exam_participants.judoka_id_participant=? group by exam.id,exam_participants.received_grade order by exam.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_examiner"){
            stmt = con->prepareStatement("select exam.id, exam.date from exam inner join exam_examiners on exam.id=exam_examiners.exam_id where exam_examiners.judoka_id_examiner=? group by exam.id order by exam.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_info"){
            stmt = con->prepareStatement("select judoka.id, judoka.name, judoka.prename,judoka.grade, judoka.pass, judoka.wkl, judoka.contact, judoka.birth_date, judoka.dsgvo_photo, judoka.examiner, judoka.comment, group_concat(qualification.name separator ';') as license from judoka left join qualification_judoka on qualification_judoka.judoka_id=judoka.id left join qualification on qualification.id=qualification_judoka.qualification_id  where judoka.id=? group by judoka.id;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="judoka_get_licenses"){
            stmt = con->prepareStatement("select qualification_judoka.qualification_id, qualification.name from judoka inner join qualification_judoka on judoka.id=qualification_judoka.judoka_id inner join qualification on qualification_judoka.qualification_id=qualification.id where judoka.id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="edit_judoka"){
            stmt = con->prepareStatement("update judoka set name=?,prename=?,grade=?,pass=?,wkl=?,contact=?,birth_date=?,dsgvo_photo=?,examiner=?,comment=? where id=?;");
            has_arguments = true;
        }else if(type=="judoka_delete_qualification"){
            stmt = con->prepareStatement("delete from qualification_judoka where judoka_id=?;");
            has_arguments = true;
        }else if(type=="judoka_add_qualification"){
            stmt = con->prepareStatement("insert into qualification_judoka (qualification_id, judoka_id) values (?, (select id from judoka where prename=? and name=?));");
            has_arguments = true;
        }else if(type=="edit_training"){
            stmt = con->prepareStatement("update training set date=?,slot_id=?,plan=?,comment=? where id=?;");
            has_arguments = true;
        }else if(type=="delete_training_trainer"){
            stmt = con->prepareStatement("delete from training_trainer where training_id =?;");
            has_arguments = true;
        }else if(type=="add_training_trainer"){
            stmt = con->prepareStatement("insert into training_trainer (training_id, judoka_id_trainer) values (?,?);");
            has_arguments = true;
        }else if(type=="delete_training_participants_readable"){
            stmt = con->prepareStatement("delete from training_participants where training_id =? and judoka_id_participant in (select judoka.id from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where sessions.id =?);");
            has_arguments = true;
        }else if(type=="add_training_participants"){
            stmt = con->prepareStatement("insert into training_participants (training_id, judoka_id_participant) values (?,?);");
            has_arguments = true;
        }else if(type=="add_training"){
            stmt = con->prepareStatement("insert into training (date,slot_id,plan,comment) values (?,?,?,?);");
            has_arguments = true;
            returns_id = true;
        }else if(type=="add_exam"){
            stmt = con->prepareStatement("insert into exam (date, begin, end, duration) values (?,?,?,?);");
            has_arguments = true;
            returns_id = true;
        }else if(type=="add_exam_examiners"){
            stmt = con->prepareStatement("insert into exam_examiners (exam_id, judoka_id_examiner) values (?,?);");
            has_arguments = true;
        }else if(type=="add_exam_participants"){
            stmt = con->prepareStatement("insert into exam_participants (exam_id, judoka_id_participant,received_grade) values (?,?,?);");
            has_arguments = true;
        }else if(type=="update_judoka_grade"){
            stmt = con->prepareStatement("update judoka set grade=? where id=?;");
            has_arguments = true;
        }else if(type=="submit_request"){
            stmt = con->prepareStatement("insert into request (user_id_issuer,datetime_issued,text,done) values ((select user_id from sessions where id=?),NOW(),?,0);");
            has_arguments = true;
        }else if(type=="edit_comp"){
            stmt = con->prepareStatement("update competition set date =?, name=?,location=? where id=?;");
            has_arguments = true;
        }else if(type=="delete_comp_coach"){
            stmt = con->prepareStatement("delete from competition_coachs where competition_id=?;");
            has_arguments = true;
        }else if(type=="add_comp_coach"){
            stmt = con->prepareStatement("insert into competition_coachs (competition_id, judoka_id_coach) values (?,?);");
            has_arguments = true;
        }else if(type=="delete_comp_participant_readable"){
            stmt = con->prepareStatement("delete from competition_participants where competition_id =? and judoka_id_participant in (select judoka.id from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where sessions.id =?);");
            has_arguments = true;
        }else if(type=="add_comp_participant"){
            stmt = con->prepareStatement("insert into competition_participants (competition_id, judoka_id_participant,class,result,comment) values (?,?,?,?,?);");
            has_arguments = true;
        }else if(type=="add_comp"){
            stmt = con->prepareStatement("insert into competition (date, name,location) values (?,?,?);");
            has_arguments = true;
            returns_id = true;
        }else if(type=="edit_exam"){
            stmt = con->prepareStatement("update exam set date=?,begin=?,end=?,duration=? where id=?;");
            has_arguments = true;
        }else if(type=="delete_exam_participant_readable"){
            stmt = con->prepareStatement("delete from exam_participants where exam_id=? and judoka_id_participant in (select judoka.id from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where sessions.id =?);");
            has_arguments = true;
        }else if(type=="delete_exam_examiner"){
            stmt = con->prepareStatement("delete from exam_examiners where exam_id=?;");
            has_arguments = true;
        }else if(type=="edit_user"){
            stmt = con->prepareStatement("update user set username=?, pwd_hash=AES_ENCRYPT(?, UNHEX(SHA2(?,512))) where id=(select user_id from sessions where id=?);");
            has_arguments = true;
        }else if(type=="show_licenses"){
            stmt = con->prepareStatement("select id, name, rate from qualification;");
            returns_result = true;
        }else if(type=="get_examiner_all"){
            stmt = con->prepareStatement("select id,prename,name from judoka where examiner=1;");
            returns_result = true;
        }else if(type=="show_groups"){
            stmt = con->prepareStatement("select id, group_name from `groups`;");
            returns_result = true;
        }else if(type=="show_slot"){
            stmt = con->prepareStatement("select id, name,begin,end,duration,rate,active from slots;");
            returns_result = true;
        }else if(type=="show_user"){
            stmt = con->prepareStatement("select user.id, user.username,judoka.name,judoka.prename from user inner join judoka on judoka.id=user.assoc_judoka_id;");
            returns_result = true;
        }else if(type=="exam_get_examiner"){
            stmt = con->prepareStatement("select exam_examiners.exam_id,exam_examiners.judoka_id_examiner,judoka.name, judoka.prename from exam_examiners inner join judoka on exam_examiners.judoka_id_examiner=judoka.id where exam_examiners.exam_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="training_get_trainer"){
            stmt = con->prepareStatement("select training_trainer.training_id,training_trainer.judoka_id_trainer, judoka.prename, judoka.name from training_trainer inner join judoka on training_trainer.judoka_id_trainer=judoka.id where training_trainer.training_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="training_get_info"){
            stmt = con->prepareStatement("select date,slot_id,plan,comment from training where id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_comp_info"){
            stmt = con->prepareStatement("select name,date,location from competition where id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_coach_comp"){
            stmt = con->prepareStatement("select competition_coachs.competition_id,judoka.id,judoka.name,judoka.prename from judoka inner join competition_coachs on competition_coachs.judoka_id_coach=judoka.id where competition_coachs.competition_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_requests_admin"){
            stmt = con->prepareStatement("select request.id, request.datetime_issued, request.text, request.done, user.username from request inner join user on request.user_id_issuer=user.id order by request.datetime_issued desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_license_info"){
            stmt = con->prepareStatement("select id, name, rate from qualification where id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_group_info"){
            stmt = con->prepareStatement("select id, group_name from `groups` where id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_group_members"){
            stmt = con->prepareStatement("select groups_user.user_id, groups_user.group_id, user.username from groups_user inner join user on user.id=groups_user.user_id where groups_user.group_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_group_permissions"){
            stmt = con->prepareStatement("select permissions.readable_judoka_id, permissions.group_id, judoka.prename, judoka.name from permissions inner join judoka on judoka.id=permissions.readable_judoka_id where permissions.group_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_slot_info"){
            stmt = con->prepareStatement("select id, name,begin,end,duration,rate,active from slots where id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_user_info_pro"){
            stmt = con->prepareStatement("select user.id, user.username,user.assoc_judoka_id from user where id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_user_groups"){
            stmt = con->prepareStatement("select group_id from groups_user where user_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="training_get_participants"){
            stmt = con->prepareStatement("select training_participants.training_id,training_participants.judoka_id_participant,judoka.prename,judoka.name from training_participants inner join judoka on training_participants.judoka_id_participant=judoka.id inner join permissions on training_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id where groups_user.user_id=(select user_id from sessions where sessions.id=?) and training_participants.training_id=? group by training_participants.judoka_id_participant, training_participants.training_id;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_comp_participants"){
            stmt = con->prepareStatement("select competition_participants.competition_id, judoka.id,judoka.name,judoka.prename,competition_participants.class,competition_participants.result,competition_participants.comment from judoka inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join permissions on competition_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id where groups_user.user_id=(select user_id from sessions where sessions.id=?) and competition_participants.competition_id=? group by judoka.id, competition_participants.class, competition_participants.result, competition_participants.comment, competition_participants.competition_id;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_exam_participants"){
            stmt = con->prepareStatement("select exam_participants.exam_id,judoka.id, judoka.name, judoka.prename, exam_participants.received_grade from exam_participants inner join judoka on exam_participants.judoka_id_participant=judoka.id inner join permissions on exam_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id where groups_user.user_id=(select user_id from sessions where sessions.id=?) and exam_participants.exam_id =? group by judoka.id,exam_participants.received_grade;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_user_info"){
            stmt = con->prepareStatement("select user.username from user inner join sessions on sessions.user_id=user.id where sessions.id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_training"){
            stmt = con->prepareStatement("select training.id,training.date,training.slot_id,training.plan,training.comment, slots.name, group_concat(distinct judoka.prename,' ',judoka.name separator ',') as trainer, group_concat(distinct judoka1.prename,' ',judoka1.name separator ', ') as participants  from training inner join slots on training.slot_id=slots.id inner join training_participants on training.id=training_participants.training_id inner join permissions on training_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id inner join training_trainer on training.id=training_trainer.training_id inner join judoka on judoka.id=training_trainer.judoka_id_trainer inner join judoka as judoka1 on judoka1.id=training_participants.judoka_id_participant where  groups_user.user_id=(select user_id from sessions where sessions.id=?) group by training.id order by training.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_exam"){
            stmt = con->prepareStatement("select exam.id,exam.date, exam.begin, exam.end, exam.duration, group_concat(distinct judoka.prename,' ',judoka.name separator ',') as examiner, group_concat(distinct judoka1.prename,' ',judoka1.name,', ', abs(exam_participants.received_grade),if( exam_participants.received_grade>0,'.Kyu','.Dan') separator '; ') as examined from exam  inner join exam_participants on exam.id=exam_participants.exam_id inner join exam_examiners on exam.id=exam_examiners.exam_id inner join judoka on exam_examiners.judoka_id_examiner=judoka.id inner join judoka as judoka1 on exam_participants.judoka_id_participant=judoka1.id inner join permissions on exam_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id where groups_user.user_id=(select user_id from sessions where sessions.id=?) group by exam.id order by exam.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="show_comp"){
            stmt = con->prepareStatement("select competition.id,competition.date,competition.name,competition.location from competition inner join competition_participants on competition.id=competition_participants.competition_id inner join permissions on competition_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id where groups_user.user_id=(select user_id from sessions where sessions.id=?) group by competition.id order by competition.date desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="delete_training"){
            stmt = con->prepareStatement("delete from training where id=?;");
            has_arguments = true;
        }else if(type=="delete_training_participants"){
            stmt = con->prepareStatement("delete from training_participants where training_id=?;");
            has_arguments = true;
        }else if(type=="delete_exam"){
            stmt = con->prepareStatement("delete from exam where id=?;");
            has_arguments = true;
        }else if(type=="delete_exam_participants"){
            stmt = con->prepareStatement("delete from exam_participants where exam_id=?;");
            has_arguments = true;
        }else if(type=="delete_comp"){
            stmt = con->prepareStatement("delete from competition where id=?;");
            has_arguments = true;
       }else if(type=="delete_comp_participants"){
            stmt = con->prepareStatement("delete from competition_participants where comp_id=?;");
            has_arguments = true;
        }else if(type=="show_requests"){
            stmt = con->prepareStatement("select request.id, request.datetime_issued,request.text, request.done from request inner join sessions on sessions.user_id=request.user_id_issuer where sessions.id=? order by request.datetime_issued desc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="delete_judoka"){
            stmt = con->prepareStatement("delete from judoka where id=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_permissions"){
            stmt = con->prepareStatement("delete from permissions where readable_judoka_id=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_training_participation"){
            stmt = con->prepareStatement("delete from training_participants where judoka_id_participant=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_exam_participation"){
            stmt = con->prepareStatement("delete from exam_participants where judoka_id_participant=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_comp_participation"){
            stmt = con->prepareStatement("delete from competition_participants where judoka_id_participant=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_groups"){
            stmt= con->prepareStatement("delete from groups_user where user_id=(select id from user where assoc_judoka_id=?);");
            has_arguments = true;
        }else if(type=="delete_judoka_messages"){
            stmt= con->prepareStatement("delete from request where user_id_issuer=(select id from user where assoc_judoka_id=?);");
            has_arguments = true;
        }else if(type=="delete_judoka_user"){
            stmt = con->prepareStatement("delete from user where assoc_judoka_id=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_trainer"){
            stmt = con->prepareStatement("delete from training_trainer where judoka_id_trainer=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_examiner"){
            stmt = con->prepareStatement("delete from exam_examiners where judoka_id_examiner=?;");
            has_arguments = true;
        }else if(type=="delete_judoka_coach"){
            stmt = con->prepareStatement("delete from competition_coachs where judoka_id_coach=?;");
            has_arguments = true;
        }else if(type=="add_group"){
            stmt = con->prepareStatement("insert into `groups` (group_name) values (?);");
            returns_id = true;
            has_arguments = true;
        }else if(type=="add_user_group"){
            stmt = con->prepareStatement("insert into groups_user (group_id, user_id) values (?,?);");
            has_arguments = true;
        }else if(type=="add_group_permission"){
            stmt = con->prepareStatement("insert into permissions (group_id, readable_judoka_id) values (?,?);");
            has_arguments = true;
        }else if(type=="delete_empty_training"){
            stmt = con->prepareStatement("delete from training where training.id not in (select training_id from training_participants) and training.id not in (select training_id from training_trainer);");
        }else if(type=="delete_empty_comp"){
            stmt = con->prepareStatement("delete from competition where competition.id not in (select competition_id from competition_participants) and competition.id not in (select competition_id from competition_coachs);");
        }else if(type=="delete_empty_exam"){
            stmt = con->prepareStatement("delete from exam where exam.id not in (select exam_id from exam_participants) and exam.id not in (select exam_id from exam_examiners);");
        }else if(type=="show_judoka_delete"){
            stmt = con->prepareStatement("select judoka.id, judoka.prename, judoka.name, judoka.birth_date from judoka where judoka.id not in (select judoka.id from judoka inner join exam_participants on exam_participants.judoka_id_participant=judoka.id inner join exam as examp on exam_participants.exam_id=examp.id where examp.date>? union select judoka.id from judoka inner join exam_examiners on judoka.id=exam_examiners.judoka_id_examiner inner join exam as exame on exame.id=exam_examiners.exam_id where exame.date>? union select judoka.id from judoka inner join training_participants on training_participants.judoka_id_participant=judoka.id inner join training as trainingp on training_participants.training_id=trainingp.id where trainingp.date>? union select judoka.id from judoka inner join training_trainer on training_trainer.judoka_id_trainer=judoka.id inner join training as trainingt on trainingt.id=training_trainer.training_id where trainingt.date>? union select judoka.id from judoka inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition as compp on competition_participants.competition_id=compp.id where compp.date>? union select judoka.id from judoka inner join competition_coachs on competition_coachs.judoka_id_coach=judoka.id inner join competition as compc on competition_coachs.competition_id=compc.id where compc.date>?);");
            has_arguments = true;
            returns_result = true;
        }else if(type=="delete_group_permissions"){
            stmt = con->prepareStatement("delete from permissions where group_id=?;");
            has_arguments = true;
        }else if(type=="delete_group_users"){
            stmt = con->prepareStatement("delete from groups_user where group_id=?;");
            has_arguments = true;
        }else if(type=="delete_group"){
            stmt = con->prepareStatement("delete from `groups` where id=?;");
            has_arguments = true;
        }else if(type=="delete_license"){
            stmt = con->prepareStatement("delete from qualification where id=?;");
            has_arguments = true;
        }else if(type=="delete_license_judoka"){
            stmt = con->prepareStatement("delete from qualification_judoka where qualification_id=?;");
            has_arguments = true;
        }else if(type=="delete_slot"){
            stmt = con->prepareStatement("delete from slot where id=?;");
            has_arguments = true;
        }else if(type=="get_slot_training"){
            stmt = con->prepareStatement("select id from training where slot_id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_judoka_trainings"){
            stmt = con->prepareStatement("select judoka.id, judoka.name, judoka.prename, judoka.birth_date, judoka.grade, group_concat(training_participants.training_id) as trainings from judoka inner join permissions on permissions.readable_judoka_id=judoka.id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on groups_user.user_id=sessions.user_id inner join training_participants on judoka.id=training_participants.judoka_id_participant inner join training on training.id=training_participants.training_id where training.date<? and training.date>? and training.slot_id=? and sessions.id=? group by judoka.id order by judoka.prename asc, judoka.name asc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_salary_qualification"){
            stmt = con->prepareStatement("select qualification.name,qualification.rate from qualification where qualification.rate = (select max(qualification.rate) from qualification inner join qualification_judoka on qualification_judoka.qualification_id=qualification.id inner join user on user.assoc_judoka_id=qualification_judoka.judoka_id inner join sessions on sessions.user_id=user.id where sessions.id=?) limit 1;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_salary_data_competitions"){
            stmt = con->prepareStatement("select competition.id, competition.date, competition.name, slots.name as slotname, slots.rate from judoka inner join competition_coachs on competition_coachs.judoka_id_coach=judoka.id inner join competition on competition_coachs.competition_id=competition.id inner join user on user.assoc_judoka_id=judoka.id inner join sessions on sessions.user_id=user.id inner join slots where sessions.id=? and competition.date<=last_day(?) and competition.date>=? and slots.id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_salary_data_exams"){
            stmt = con->prepareStatement("select exam.id, exam.date, exam.begin, exam.end, exam.duration, slots.name, slots.rate from judoka inner join exam_examiners on exam_examiners.judoka_id_examiner=judoka.id inner join exam on exam_examiners.exam_id=exam.id inner join user on user.assoc_judoka_id=judoka.id inner join sessions on sessions.user_id=user.id inner join slots where sessions.id=? and exam.date<=last_day(?) and exam.date>=? and slots.id=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_salary_data_trainings"){
            stmt = con->prepareStatement("select training.id, training.date, slots.name, slots.begin, slots.end, slots.duration, slots.rate from judoka inner join training_trainer on training_trainer.judoka_id_trainer=judoka.id inner join training on training_trainer.training_id=training.id inner join slots on slots.id=training.slot_id inner join user on user.assoc_judoka_id=judoka.id inner join sessions on sessions.user_id=user.id where sessions.id=? and training.date<=last_day(?) and training.date>=?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_competitors_list"){
            stmt = con->prepareStatement("select judoka.id as participant_id, judoka.name,judoka.prename,judoka.birth_date,competition.name as 'compname',competition.location,competition_participants.class,competition_participants.result from competition_participants inner join judoka on competition_participants.judoka_id_participant=judoka.id inner join competition on competition_participants.competition_id=competition.id where competition.date<=? and competition.date >= ? order by judoka.prename asc, judoka.name asc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_dates_list"){
            stmt = con->prepareStatement("select training.id, training.date, slots.name from training inner join slots on training.slot_id=slots.id where slot_id=? and training.date<? and training.date>?;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_pupils_empty"){
            stmt = con->prepareStatement("select judoka.name,judoka.prename,judoka.birth_date,judoka.grade from judoka inner join training_participants on judoka.id=training_participants.judoka_id_participant inner join permissions on permissions.readable_judoka_id=judoka.id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on groups_user.user_id=sessions.user_id inner join training on training.id=training_participants.training_id where sessions.id=? and training.date<? and training.date>? and training.slot_id=? group by judoka.name, judoka.prename, judoka.birth_date, judoka.grade order by judoka.prename asc, judoka.name asc;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="add_slot"){
            stmt = con->prepareStatement("insert into slots (name,begin,end,duration,rate,active) values(?,?,?,?,?,?);");
            has_arguments = true;
        }else if(type=="edit_slot"){
            stmt = con->prepareStatement("update slots set name=?, begin=?, end=?, duration=?,  rate=?, active=? where id=?;");
            has_arguments = true;
        }else if(type=="add_license"){
            stmt = con->prepareStatement("insert into qualification (name,rate) values(?,?);");
            has_arguments = true;
        }else if(type=="edit_license"){
            stmt = con->prepareStatement("update qualification set name=?, rate=? where id=?;");
            has_arguments = true;
        }else if(type=="set_pass"){
            stmt = con->prepareStatement("update user set pwd_hash=AES_ENCRYPT(?, UNHEX(SHA2(?,512))) where id=?;");
            has_arguments = true;
        }else if(type=="edit_group"){
            stmt = con->prepareStatement("update `groups` set group_name = ? where id=?;");
            has_arguments = true;
        }else if(type=="delete_user_group"){
            stmt = con->prepareStatement("delete from groups_user where user_id=?;");
            has_arguments = true;
        }else if(type=="edit_user_admin"){
            stmt = con->prepareStatement("update user set username=?,assoc_judoka_id=?, pwd_hash=AES_ENCRYPT(?, UNHEX(SHA2(?,512))) where id=?;");
            has_arguments = true;
        }else if(type=="delete_user"){
            stmt = con->prepareStatement("delete from user where id=?;");
            has_arguments = true;
        }else if(type=="delete_user_requests"){
            stmt = con->prepareStatement("delete from request where user_id_issuer=?;");
            has_arguments = true;
        }else if(type=="mark_request"){
            stmt = con->prepareStatement("update request set done='1' where id=?;");
            has_arguments = true;
        }else if(type=="add_user"){
            stmt = con->prepareStatement("insert into user (username,assoc_judoka_id,pwd_hash) values (?,?,AES_ENCRYPT(?, UNHEX(SHA2(?,512))));");
            returns_single = true;
            has_arguments = true;
        }else if(type=="get_judoka_age"){
            stmt = con->prepareStatement("select (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 10 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 4 year)),'-01-01') and sessions.id=?) as U10, (select count(*)  from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 12 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 10 year)),'-01-01') and sessions.id=?) as U12, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 15 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 12 year)),'-01-01') and sessions.id=?) as U15, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 18 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 15 year)),'-01-01') and sessions.id=?) as U18, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 21 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 18 year)),'-01-01') and sessions.id=?) as U21, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date<concat(year(date_sub(curdate(), interval 21 year)),'-01-01') and sessions.id=?) as Aktive;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_competitors_age"){
            stmt = con->prepareStatement("select (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition on competition.id=competition_participants.competition_id where competition.date >=concat(year(curdate()),'-01-01') and birth_date>concat(year(date_sub(curdate(), interval 10 year)),'-12-31') and birth_date<=concat(year(date_sub(curdate(), interval 4 year)),'-12-31') and sessions.id=?) as U10, (select count(*)  from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition on competition.id=competition_participants.competition_id where competition.date >=concat(year(curdate()),'-01-01') and birth_date>concat(year(date_sub(curdate(), interval 12 year)),'-12-31') and birth_date<=concat(year(date_sub(curdate(), interval 10 year)),'-12-31') and sessions.id=?) as U12, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition on competition.id=competition_participants.competition_id where competition.date >=concat(year(curdate()),'-01-01') and birth_date>concat(year(date_sub(curdate(), interval 15 year)),'-12-31') and birth_date<=concat(year(date_sub(curdate(), interval 12 year)),'-12-31') and sessions.id=?) as U15, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition on competition.id=competition_participants.competition_id where competition.date >=concat(year(curdate()),'-01-01') and birth_date>concat(year(date_sub(curdate(), interval 18 year)),'-12-31') and birth_date<=concat(year(date_sub(curdate(), interval 15 year)),'-12-31') and sessions.id=?) as U18, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition on competition.id=competition_participants.competition_id where competition.date >=concat(year(curdate()),'-01-01') and birth_date>concat(year(date_sub(curdate(), interval 21 year)),'-12-31') and birth_date<=concat(year(date_sub(curdate(), interval 18 year)),'-12-31') and sessions.id=?) as U21, (select count(*) from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id inner join competition_participants on competition_participants.judoka_id_participant=judoka.id inner join competition on competition.id=competition_participants.competition_id where competition.date >=concat(year(curdate()),'-01-01') and birth_date<=concat(year(date_sub(curdate(), interval 21 year)),'-12-31') and sessions.id=?) as Aktive;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_average_slot"){
            stmt = con->prepareStatement("select avg(cnt) as average, t.id from (select count(*) as cnt,slots.name as id from training_participants inner join training on training_participants.training_id=training.id inner join slots on training.slot_id=slots.id where training.date>=concat(year(curdate()), '-01-01') group by training.slot_id,training_participants.training_id) as t group by t.id;");
            returns_result = true;
         }else if(type=="get_grades_age"){
            stmt = con->prepareStatement("select (select group_concat(cnt) cnt from(select concat(grade,':',count(*)) cnt from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 10 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 4 year)),'-01-01') and sessions.id=? group by grade) as t) as U10, (select group_concat(cnt) cnt from(select concat(grade,':',count(*)) cnt from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 12 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 10 year)),'-01-01') and sessions.id=? group by grade) as t) as U12, (select group_concat(cnt) cnt from(select concat(grade,':',count(*)) cnt from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 15 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 12 year)),'-01-01') and sessions.id=? group by grade) as t) as U15, (select group_concat(cnt) cnt from(select concat(grade,':',count(*)) cnt from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 18 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 15 year)),'-01-01') and sessions.id=? group by grade) as t) as U18, (select group_concat(cnt) cnt from(select concat(grade,':',count(*)) cnt from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date>=concat(year(date_sub(curdate(), interval 21 year)),'-01-01') and birth_date<concat(year(date_sub(curdate(), interval 18 year)),'-01-01') and sessions.id=? group by grade) as t) as U21, (select group_concat(cnt) cnt from(select concat(grade,':',count(*)) cnt from judoka inner join permissions on judoka.id=permissions.readable_judoka_id inner join groups_user on permissions.group_id=groups_user.group_id inner join sessions on sessions.user_id=groups_user.user_id where birth_date<concat(year(date_sub(curdate(), interval 21 year)),'-01-01') and sessions.id=? group by grade) as t) as Aktive;");
            returns_result = true;
            has_arguments = true;
        }else if(type=="get_count_licenses"){
            stmt = con->prepareStatement("select count(*) as nr, qualification.name, qualification.id from qualification_judoka inner join qualification on qualification.id=qualification_judoka.qualification_id group by qualification_id;");
            returns_result = true;
         }else if(type=="get_exam_grades"){
           stmt= con->prepareStatement("select group_concat(cnt) cnt from (select concat(exam_participants.received_grade,':',count(*)) cnt from exam_participants inner join exam on exam.id=exam_participants.exam_id where exam.date>=concat(year(curdate()), '-01-01') group by exam_participants.received_grade) as t;");
           returns_result = true;
         }else if(type=="get_competition_results"){
           stmt= con->prepareStatement("select competition_participants.result,count(*) as nr from competition_participants inner join competition on competition.id=competition_participants.competition_id where competition.date>=concat(year(curdate()), '-01-01') group by competition_participants.result;");
           returns_result = true;
         }else if(type=="show_user_info"){
           stmt= con->prepareStatement("select group_concat(`groups`.group_name separator ',') as usergroups, user.username, concat(judoka.prename, ' ', judoka.name) as name from user inner join judoka on user.assoc_judoka_id=judoka.id inner join groups_user on groups_user.user_id=user.id inner join `groups` on groups_user.group_id = `groups`.id  inner join sessions on sessions.user_id=user.id where sessions.id=? group by user.id");
           has_arguments = true;
           returns_result = true;
        }else if(type=="get_exam_list"){
            stmt = con->prepareStatement("select judoka.id, judoka.name, judoka.prename,judoka.pass, judoka.birth_date, exam_participants.received_grade, (select MAX(exam1.date) from exam as exam1 inner join exam_participants as exam2 on exam1.id=exam2.exam_id inner join judoka as exam3 on exam2.judoka_id_participant=exam3.id where exam3.id=judoka.id and exam1.date<exam.date) as last_date_exam from exam inner join exam_participants on exam.id=exam_participants.exam_id inner join judoka on exam_participants.judoka_id_participant=judoka.id inner join permissions on exam_participants.judoka_id_participant=permissions.readable_judoka_id inner join groups_user on groups_user.group_id=permissions.group_id where groups_user.user_id=(select user_id from sessions where sessions.id=?) and exam.id=?;");
            returns_result = true;
            has_arguments = true;
         }
        
        if(has_arguments){
            for(int i=0; i<arguments.size(); i++){
                stmt->setString((i+1),arguments[i]);
            }
        }
        res = stmt->executeQuery();
        if(returns_single){
            while (res->next()) {
                result.append(res->getString(1));
            }
        }else if(returns_result){
            res_meta = res->getMetaData();
            int columncount = res_meta->getColumnCount();
            if(res->next()){
                res->previous();
                result.append("[");
            }else{
                BOOST_LOG_TRIVIAL(warning) <<"empty result returned!\n";
                return "{\"empty\":\"1\"}";
            }
            while (res->next()) {
                result.append("{");
                for(int i = 0; i < columncount; i++){
                    result.append("\"");
                    result.append(res_meta->getColumnLabel(i+1));
                    result.append("\":\"");
                    result.append(urlEncode(res->getString(i+1)));
                    result.append("\"");
                    if(i< columncount-1){
                        result.append(",");
                    }
                }
                result.append("},");
            }
            //remove , after last }
            result.pop_back();
            result.append("]");
        }else if(returns_id){
            sql::Statement *statement;
            statement = con->createStatement();
            res = statement->executeQuery("select last_insert_id();");
            while (res->next()) {
                result.append(res->getString(1));
            }
            statement->close();
            delete statement;
        }else{
            res->close();
            delete res;
            stmt->close();
            delete stmt;
            con->close();
            delete con;
            driver->threadEnd();
            return "1";
        }
        if(log_result && result != ""){
            BOOST_LOG_TRIVIAL(info) <<result<<std::endl;
        }
        res->close();
        delete res;
        stmt->close();
        delete stmt;
        con->close();
        delete con;
        driver->threadEnd();
        return result;
    } catch (sql::SQLException &e) {
        BOOST_LOG_TRIVIAL(error)  << "# ERR: SQLException in " << __FILE__;
        BOOST_LOG_TRIVIAL(error)  << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        BOOST_LOG_TRIVIAL(error)  << "# ERR: " << e.what();
        BOOST_LOG_TRIVIAL(error)  << " (MySQL error code: " << e.getErrorCode();
        BOOST_LOG_TRIVIAL(error)  << ", SQLState: " << e.getSQLState() << " )" << endl;
        return "0";
    }
    return "1";
}

boost::beast::http::response<boost::beast::http::string_body> Utility::createResponse(std::string &request, unsigned version) {
    http::response<http::string_body> res{ http::status::ok,version};
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "application/json; charset=utf-8");
    res.set(http::field::access_control_allow_origin, "*");
    srand(time(0));
    std::time_t time_now = std::time(nullptr);
    std::regex regex ("pass=\\w+\\b");
    BOOST_LOG_TRIVIAL(info) << "Request: " << std::regex_replace(request,regex,"pass=xxxx") << endl;
    std::string success,success0,success1,success2,success3,success4,success5,success6,success7,success8,success9 = "1";
    std::string admin = "0";
    std::string userid = "0";
    std::string result = "";
    std::string reason = "";
    std::string query = "";
    std::string type = "";
    std::string judoka_name, judoka_prename, judoka_dob, judoka_graduation, judoka_contact, judoka_wkl, judoka_pass, judoka_license, judoka_examiner,judoka_dsgvo, date, training_slot, trainer, pupils, plan, comment, list_begin, list_end, examiner, examined, comp_name, coach, location, results, year, year_end, username, configpath, password, request_body,slot_id, month,judoka_id,sessionid,id,name,begin,end,duration,groups,members,readable,rate,active = "";
    //parse request and create query
    std::vector<std::string> arguments = splitString(request, '&');
    std::vector<std::string> sql_args;
    for (auto &args: arguments){
        if(args.find('=') != std::string::npos){
            std::vector<std::string> content = splitString(args, '=');
            if(content.front() == "type"){
                type = content.back();
            }else if(content.front() == "username"){
                username = content.back();
            }else if(content.front() == "pass"){
                password = content.back();
            }else if(content.front() == "judoka_name"){
                judoka_name = clearFormatting(content.back());
            }else if(content.front() == "judoka_prename"){
                judoka_prename = clearFormatting(content.back());
            }else if(content.front() == "judoka_birthdate"){
                judoka_dob = content.back();
            }else if(content.front() == "judoka_graduation"){
                judoka_graduation = content.back();
            }else if(content.front() == "judoka_id"){
                judoka_id = content.back();
                BOOST_LOG_TRIVIAL(info) << "Judokaid:" << judoka_id << std::endl;
            }else if(content.front() == "judoka_contact"){
                judoka_contact = clearFormatting(content.back());
                if(judoka_contact=="judoka_contact"){
                    judoka_contact='-';
                }
            }else if(content.front() == "judoka_wkl"){
                judoka_wkl = content.back();
                if(judoka_wkl=="judoka_wkl"){
                    judoka_wkl="-";
                }
            }else if(content.front() == "judoka_pass"){
                judoka_pass = content.back();
                if(judoka_pass=="judoka_pass"){
                    judoka_pass='-';
                }
            }else if(content.front() == "judoka_license"){
                judoka_license = clearFormatting(content.back());
            }else if(content.front() == "judoka_examiner"){
                judoka_examiner = content.back();
            }else if(content.front() == "judoka_dsgvo"){
                judoka_dsgvo = content.back();
            }else if(content.front() == "date"){
                date = content.back();
            }else if(content.front() == "training_slot"){
                training_slot = content.back();
            }else if(content.front() == "trainer"){
                trainer = clearFormatting(content.back());
            }else if(content.front() == "pupils"){
                pupils = clearFormatting(content.back());
            }else if(content.front() == "plan"){
                plan = clearFormatting(content.back());
                if(plan=="plan"){
                    plan='-';
                }
            }else if(content.front() == "judoka_comment" || content.front() == "training_comment"){
                comment = clearFormatting(content.back());
                if(comment=="judoka_comment" || comment=="training_comment"){
                    comment='-';
                }
            }else if(content.front() == "list_begin"){
                list_begin = content.back();
            }else if(content.front() == "list_end"){
                list_end = content.back();
            }else if(content.front() == "examiner"){
                examiner = clearFormatting(content.back());
            }else if(content.front() == "examined"){
                examined = clearFormatting(content.back());
            }else if(content.front() == "comp_name"){
                comp_name = clearFormatting(content.back());
            }else if(content.front() == "coach"){
                coach = clearFormatting(content.back());
            }else if(content.front() == "location"){
                location = clearFormatting(content.back());
            }else if(content.front() == "results"){
                results = clearFormatting(content.back());
            }else if(content.front() == "year"){
                year = content.back();
            }else if(content.front() == "configpath"){
                configpath = content.back();
            }else if(content.front() == "request"){
                request_body = clearFormatting(content.back());
            }else if(content.front() == "slot_id"){
                slot_id = content.back();
            }else if(content.front() == "id"){
                id = content.back();
            }else if(content.front() == "month"){
                month = content.back();
                month.append("-01");
            }else if(content.front() == "name"){
                name = clearFormatting(content.back());
            }else if(content.front() == "duration"){
                duration = content.back();
            }else if(content.front() == "begin"){
                begin = clearFormatting(content.back());
            }else if(content.front() == "end"){
                end = clearFormatting(content.back());
            }else if(content.front() == "groups"){
                groups = clearFormatting(content.back());
            }else if(content.front() == "members"){
                members = clearFormatting(content.back());
            }else if(content.front() == "readable"){
                readable = clearFormatting(content.back());
            }else if(content.front() == "rate"){
                rate = content.back();
            }else if(content.front() == "active"){
                active = content.back();
            }else if(content.front() == "session"){
                sessionid = content.back();
                //check, if session is valid
                sql_args.push_back(sessionid);
                result = executeSQL("check_session",sql_args);
                if(result != ""){
                    userid = result;
                    success = executeSQL("update_session",sql_args);
                    sql_args.clear();
                }else{
                    success = executeSQL("delete_session",sql_args);
                    sql_args.clear();
                    BOOST_LOG_TRIVIAL(info)  << "session: "<< sessionid << " no longer valid" << std::endl;
                    res.result(boost::beast::http::status::internal_server_error);
                    res.body() = "{\"sessionid\":0, \"reason\":\"session not valid\"}";
                    return res;
                }
            }else{
                BOOST_LOG_TRIVIAL(warning) << "unrecognized API call:"<< content.front() << ":" <<content.back() <<std::endl;
                res.result(boost::beast::http::status::internal_server_error);
                res.body() = ("{\"success\":0,\"reason\":\"Unrecognized API call\"}");
                return res;
            }
        }
    }
    
    if(sessionid== "" && type != "login"){
        res.result(boost::beast::http::status::internal_server_error);
        res.body() = "{\"sessionid\":0, \"reason\":\"sessionid not given\"}";
        return res;
    }
    
    if(type == "login"){
        sql_args = {username,password,password};
        sessionid = executeSQL(type,sql_args);
        if(sessionid == ""){
            //check combination of username and password:
            result = executeSQL("select_userid",sql_args);
            sql_args.clear();
            if(result != ""){
                sessionid = to_string(rand());
                sql_args = {sessionid,result};
                success = executeSQL("new_session",sql_args);
                sql_args.pop_back();
                admin = executeSQL("check_admin",sql_args);
                sql_args.clear();
                if(success=="1"){
                    if(admin != ""){
                        res.result(boost::beast::http::status::ok);
                        res.body() = ("{\"sessionid\":" + sessionid +", \"admin\":" + admin +"}");
                    }else{
                        res.result(boost::beast::http::status::ok);
                        res.body() = ("{\"sessionid\":" + sessionid +", \"admin\": 0}");
                    }
                }else{
                    res.result(boost::beast::http::status::internal_server_error);
                    res.body() = "{\"sessionid\":0 , \"reason\":\"session generation failed\"}";
                }
            }else{
                res.result(boost::beast::http::status::unauthorized);
                BOOST_LOG_TRIVIAL(warning)  << "failed login" <<endl;
                res.body() = "{\"sessionid\":0, \"reason\":\"username and password mismatch\"}";
            }
        }else{
            sql_args.clear();
            sql_args.push_back(sessionid);
            admin = executeSQL("check_admin",sql_args);
            sql_args.clear();
            res.result(boost::beast::http::status::ok);
            res.body() = ("{\"sessionid\":"+sessionid+", \"admin\":" + admin +"}");
        }
    }else if(type=="logout"){
        sql_args.push_back(sessionid);
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() = "{\"success\": " + success + "}";
    }else if(type=="show_judoka"){
        sql_args.push_back(userid);
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="show_judoka_sort"){
        sql_args.push_back(userid);
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="show_judoka_all"){
        result = executeSQL(type,sql_args);
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="add_judoka"){
        result = "";
        sql_args = {judoka_name,judoka_prename,judoka_dob};
        result = executeSQL("check_judoka_duplicate",sql_args);
        sql_args.clear();
        if(result != ""){
            res.result(boost::beast::http::status::internal_server_error);
            res.body() =("{\"success\": 0 ,\"reason\":\"judoka duplicate\"}");
        }else{
            sql_args =  {judoka_name,judoka_prename,judoka_graduation,judoka_pass,judoka_wkl,judoka_contact,judoka_dob,judoka_dsgvo,judoka_examiner,comment};
            success = executeSQL(type,sql_args);
            sql_args.clear();
            if(success=="1"){
                if(judoka_license!="judoka_license"){
                    vector <string> qualifications = splitString(judoka_license, ' ');
                    for(vector<string>::iterator i = qualifications.begin(); i!=qualifications.end(); i++){
                        sql_args = {*i,judoka_prename,judoka_name};
                        success0 = executeSQL("add_qualifications_judoka",sql_args);
                        sql_args.clear();
                    }
                }else{
                    success0 = "1";
                }
                sql_args = {judoka_prename,judoka_name};
                success1 = executeSQL("add_permissions_judoka",sql_args);
                sql_args.clear();
            }
            res.result(set_status(min({success,success0,success1})));
            res.body() =("{\"success\": " + min({success,success0,success1}) + ",\"reason\":\"judoka not added\"}");
        }
    }else if(type == "get_qualification_all" || type=="get_examiner_all" || type == "show_licenses" || type == "show_groups"|| type == "show_slot" || type == "get_slots_all" || type == "get_slots_active"|| type=="get_trainer_all" || type=="show_slot" || type=="show_user"||type=="show_requests_admin"){
        sql_args = {};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="exam_get_examiner" || type == "training_get_info" || type=="training_get_trainer" || type == "get_comp_info" || type=="get_coach_comp" || type=="get_license_info" || type=="get_group_info" || type=="get_group_members" ||type=="get_group_permissions" || type=="get_slot_info" || type=="get_user_info_pro" || type=="get_user_groups"){
        sql_args.push_back(id);
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="training_get_participants" || type=="get_comp_participants" || type=="show_exam_participants"|| type=="get_exam_list"){
        sql_args = {sessionid,id};
        result = executeSQL(type,sql_args);
        sql_args.clear();    
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_user_info" || type=="show_training" || type=="show_exam" || type=="show_comp"|| type=="show_requests" || type=="get_salary_qualification"){
        sql_args.push_back(sessionid);
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_judoka_age" || type=="get_grades_age" || type=="get_competitors_age"){
        sql_args =  {sessionid,sessionid,sessionid,sessionid,sessionid,sessionid};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if (type=="judoka_get_comp" || type=="judoka_get_training"|| type=="judoka_get_exam"||type=="judoka_get_info" || type=="judoka_get_licenses" || type=="judoka_get_coach" || type=="judoka_get_examiner" || type=="judoka_get_trainer"){
        sql_args.push_back(judoka_id);
        result=executeSQL(type,sql_args);
        sql_args.clear();
        if(result !=""){
            res.result(set_status(result));
            res.body() = result;
        }else{
            res.result(boost::beast::http::status::internal_server_error);
            res.body() = ("{\"success\":0,\"reason\":\"information not found\"}");
        }
    }else if (type=="edit_judoka"){
        sql_args =  {judoka_name,judoka_prename,judoka_graduation,judoka_pass,judoka_wkl,judoka_contact,judoka_dob,judoka_dsgvo,judoka_examiner,comment,judoka_id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        if(success=="1"){
            sql_args.push_back(judoka_id);
            success = executeSQL("judoka_delete_qualification",sql_args);
            sql_args.clear();
            if(judoka_license != "judoka_license" && success == "1"){
                vector <string> qualifications = splitString(judoka_license, ' ');
                for(vector<string>::iterator i = qualifications.begin(); i!=qualifications.end(); i++){
                    sql_args = {*i,judoka_prename,judoka_name};
                    success0 = executeSQL("judoka_add_qualification",sql_args);
                    sql_args.clear();
                }
            }else{
                success0 = success1 = "1";
            }
        }
        res.result(set_status(min({success,success0})));
        res.body() = "{\"success\": " + min({success,success0}) + ",\"reason\":\"judoka not edited\"}";
    }else if (type=="edit_training"){
        sql_args = {date,training_slot,plan,comment,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        if(success=="1"){
            sql_args.push_back(id);
            success = executeSQL("delete_training_trainer",sql_args);
            sql_args.clear();
            if(success=="1"){
                vector <string> qualifications = splitString(trainer, ' ');
                for(vector<string>::iterator i = qualifications.begin(); i!=qualifications.end(); i++){
                    sql_args = {id,*i};
                    success0 = executeSQL("add_training_trainer",sql_args);
                    sql_args.clear();
                }
            }
            sql_args = {id,sessionid};
            success = executeSQL("delete_training_participants_readable",sql_args);
            sql_args.clear();
            if(success=="1"){
                vector <string> pupils_vector = splitString(pupils, ' ');
                for(vector<string>::iterator i = pupils_vector.begin(); i!=pupils_vector.end(); i++){
                    sql_args = {id,*i};
                    success1 = executeSQL("add_training_participants",sql_args);
                }
            }
        }
        res.result(set_status(min({success,success0,success1})));
        res.body() = "{\"success\": " + min({success0,success,success1}) + ",\"reason\":\"training not edited\"}";
    }else if (type=="add_training"){
        sql_args = {date,training_slot,plan,comment};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        if(result != ""){
            vector <string> qualifications = splitString(trainer, ' ');
            for(vector<string>::iterator i = qualifications.begin(); i!=qualifications.end(); i++){
                sql_args = {result,*i};
                success1 = executeSQL("add_training_trainer",sql_args);
                sql_args.clear();
            }
            vector <string> pupils_vector = splitString(pupils, ' ');
            for(vector<string>::iterator i = pupils_vector.begin(); i!=pupils_vector.end(); i++){
                sql_args = {result,*i};
                success0 = executeSQL("add_training_participants",sql_args);
                sql_args.clear();
            }
        }else{
            success="0";
        }
        res.result(set_status(min({success,success0,success1})));
        res.body() = "{\"success\": " + min({success,success0,success1}) + ",\"reason\":\"training not inserted\"}";
    }else if (type=="add_exam"){
        sql_args={date,begin,end,duration};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        if(result != ""){
            vector <string> examiners = splitString(examiner, ' ');
            for(vector<string>::iterator i = examiners.begin(); i!=examiners.end(); i++){
                sql_args={result,*i};
                success = executeSQL("add_exam_examiners",sql_args);
                sql_args.clear();
            }
            vector <string> examineds = splitString(examined, ' ');
            for(vector<string>::iterator i = examineds.begin(); i!=examineds.end(); i=i+2){
                if(*(i+1)!="9" && *(i+1)!=""){
                    sql_args={result,*i,*(i+1)};
                    success0 = executeSQL("add_exam_participants",sql_args);
                    sql_args.clear();
                    sql_args={*(i+1),*i};
                    success1 = executeSQL("update_judoka_grade",sql_args);
                    sql_args.clear();
                }
            }
        }else{
            success = "0";
        }
        res.result(set_status(min({success,success0,success1})));
        res.body() = "{\"success\": " + min({success,success0,success1}) + ",\"reason\":\"exam not inserted\"}";
    }else if (type=="submit_request"){
        sql_args={sessionid,request_body};
        success=executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() = "{\"success\": " + success + ",\"reason\":\"request not submitted\"}";
    }else if (type=="edit_comp"){
        sql_args={date,comp_name,location,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        if(success=="1"){
            sql_args.push_back(id);
            success0 = executeSQL("delete_comp_coach",sql_args);
            sql_args.clear();
            vector <string> coaches = splitString(coach, ' ');
            for(vector<string>::iterator i = coaches.begin(); i!=coaches.end(); i++){
                sql_args = {id,*i};
                success1 = executeSQL("add_comp_coach",sql_args);
                sql_args.clear();
            }
            sql_args = {id,sessionid};
            success2 = executeSQL("delete_comp_participant_readable",sql_args);
            sql_args.clear();
            vector <string> result_vector = splitString(results, '$');
            for(vector<string>::iterator i = result_vector.begin(); i!=result_vector.end(); i=i+4){
                if(*(i+1) !=""){
                    sql_args = {id,*i,*(i+1),*(i+2),*(i+3)};
                    success3 = executeSQL("add_comp_participant",sql_args);
                    sql_args.clear();
                }
            }
        }
        res.result(set_status(min({success,success0,success1,success2,success3})));
        res.body() = "{\"success\": " + min({success0,success1,success2,success3,success}) + ",\"reason\":\"competition not edited\"}";
    }else if (type=="add_comp"){
        sql_args={date,comp_name,location};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        if(result != ""){
            vector <string> coaches = splitString(coach, ' ');
            for(vector<string>::iterator i = coaches.begin(); i!=coaches.end(); i++){
                sql_args = {result,*i};
                success0 = executeSQL("add_comp_coach",sql_args);
                sql_args.clear();
            }
            vector <string> result_vector = splitString(results, '$');
            for(vector<string>::iterator i = result_vector.begin(); i!=result_vector.end(); i=i+4){
                if(*(i+1) !=""){
                    sql_args = {result,*i,*(i+1),*(i+2),*(i+3)};
                    success1 = executeSQL("add_comp_participant",sql_args);
                    sql_args.clear();
                }
            }
        }else{
            success0 = "0";
        }
        res.result(set_status(min({success0,success1})));
        res.body() = "{\"success\": " + min({success0,success1}) + ",\"reason\":\"competition not added\"}";
    }else if (type=="edit_exam"){
        sql_args={date,begin,end,duration,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        if(success=="1"){
            sql_args.push_back(id);
            success0 = executeSQL("delete_exam_examiner",sql_args);
            sql_args.clear();
            vector <string> examiners = splitString(examiner, ' ');
            for(vector<string>::iterator i = examiners.begin(); i!=examiners.end(); i++){
                sql_args = {id,*i};
                success1 = executeSQL("add_exam_examiners",sql_args);
                sql_args.clear();
            }
            sql_args = {id,sessionid};
            success1 = executeSQL("delete_exam_participant_readable",sql_args);
            sql_args.clear();
            vector <string> examineds = splitString(examined, ' ');
            for(vector<string>::iterator i = examineds.begin(); i!=examineds.end(); i=i+2){
                if(*(i+1)!="9" && *(i+1)!=""){
                    sql_args = {id,*i,*(i+1)};
                    success3 = executeSQL("add_exam_participants",sql_args);
                    sql_args.clear();
                }
            }
        }else{
            success = "0";
        }
        res.result(set_status(min({success,success1,success0,success2,success3})));
        res.body() = "{\"success\": " + min({success,success1,success0,success2,success3}) + ",\"reason\":\"exam not edited\"}";
    }else if (type=="edit_user"){
        sql_args = {username,password,password,sessionid};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() = "{\"success\": " + success + ",\"reason\":\"user not edited\"}";
    }else if(type=="delete_training"){
        sql_args.push_back(id);
        success0 = executeSQL(type,sql_args);
        success1 = executeSQL("delete_training_trainer",sql_args);
        success2 = executeSQL("delete_training_participants",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1,success2})));
        res.body() =("{\"success\":"+ min({success0,success1,success2})+",\"reason\":\"training not deleted\"}");
    }else if(type=="delete_exam"){
        sql_args.push_back(id);
        success0 = executeSQL(type,sql_args);
        success1 = executeSQL("delete_exam_examiner",sql_args);
        success2 = executeSQL("delete_exam_participants",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1,success2})));
        res.body() =("{\"success\":"+ min({success0,success1,success2})+",\"reason\":\"exam not deleted\"}");
    }else if(type=="delete_comp"){
        sql_args.push_back(id);
        success0 = executeSQL(type,sql_args);
        success1 = executeSQL("delete_comp_coach",sql_args);
        success2 = executeSQL("delete_comp_participants",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1,success2})));
        res.body() =("{\"success\":"+ min({success0,success1,success2})+",\"reason\":\"competition not deleted\"}");
    }else if(type=="delete_judoka"){
        sql_args.push_back(id);
        success = executeSQL(type,sql_args);
        success0 = executeSQL("delete_judoka_permissions",sql_args);
        success1 = executeSQL("delete_judoka_training_participation",sql_args);
        success2 = executeSQL("delete_judoka_comp_participation",sql_args);
        success3 = executeSQL("delete_judoka_exam_participation",sql_args);
        success4 = executeSQL("delete_judoka_groups",sql_args);
        success5 = executeSQL("delete_judoka_messages",sql_args);
        success6 = executeSQL("delete_judoka_user",sql_args);
        success7 = executeSQL("delete_judoka_trainer",sql_args);
        success8 = executeSQL("delete_judoka_examiner",sql_args);
        success9 = executeSQL("delete_judoka_coach",sql_args);
        success9 = executeSQL("judoka_delete_qualification",sql_args);
        sql_args.clear();
        res.result(set_status(min({success,success0,success1,success2,success3,success4,success5,success6,success7,success8,success9})));
        res.body() =("{\"success\":"+ min({success,success0,success1,success2,success3,success4,success5,success6,success7,success8,success9})+",\"reason\":\"judoka not deleted completely\"}");
    }else if(type=="add_group"){
        sql_args.push_back(name);
        result = executeSQL(type,sql_args);
        sql_args.clear();
        if(result != ""){
            success = "1";
            vector <string> members_vec = splitString(members, ' ');
            for(vector<string>::iterator i = members_vec.begin(); i!=members_vec.end(); i++){
                sql_args={result,*i};
                success1 = executeSQL("add_user_group",sql_args);
                sql_args.clear();
            }
            vector <string> readables = splitString(readable, ' ');
            for(vector<string>::iterator i = readables.begin(); i!=readables.end(); i++){
                sql_args={result,*i};
                success3 = executeSQL("add_group_permission",sql_args);
                sql_args.clear();
            }
        }else{
          success = "0";
        }
        res.result(set_status(min({success,success1,success3})));
        res.body() =("{\"success\":"+ min({success,success1,success3})+",\"reason\":\"group not added\"}");
    }else if(type=="add_user"){
        sql_args = {username,judoka_id,password,password};
        result = executeSQL(type,sql_args);
        sql_args.clear();
       if(result != ""){
            vector <string> members = splitString(groups, ' ');
            for(vector<string>::iterator i = members.begin(); i!=members.end(); i++){
                sql_args={*i,result};
                success = executeSQL("add_user_group",sql_args);
                sql_args.clear();
            }
            reason = "group designatin failed";
       }else{
         success = "0";  
         reason = "user insert failed";
       }
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\""+reason+"\"}");
    }else if(type=="mark_request"){
        sql_args.push_back(id);
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\"request not marked\"}");
    }else if(type=="edit_user_admin"){
        sql_args= {username,judoka_id,password,password,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        if(success=="1"){
            sql_args.push_back(id);
            success1 = executeSQL("delete_user_group",sql_args);
            sql_args.clear();
            vector <string> members = splitString(groups, ' ');
            for(vector<string>::iterator i = members.begin(); i!=members.end(); i++){
                sql_args={*i,id};
                success1 = executeSQL("add_user_group",sql_args);
                sql_args.clear();
            }
        }
        res.result(set_status(min({success,success1})));
        res.body() = "{\"success\": " + min({success,success1}) + ",\"reason\":\"user not updated\"}";
    }else if(type=="delete_user"){
        sql_args.push_back(id);
        success0 = executeSQL("delete_user",sql_args);
        success1 = executeSQL("delete_user_group",sql_args);
        success2 = executeSQL("delete_user_requests",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1,success2})));
        res.body() = "{\"success\": " + min({success0,success1,success2}) + ",\"reason\":\"user not deleted\"}";
    }else if(type=="edit_user_group"){
        sql_args.push_back(id);
        success0 = executeSQL("delete_user_group",sql_args);
        sql_args.clear();
        vector <string> members = splitString(groups, ' ');
        for(vector<string>::iterator i = members.begin(); i!=members.end(); i++){
            sql_args={*i,id};
            success1 = executeSQL("add_user_group",sql_args);
            sql_args.clear();
        }
        res.result(set_status(min({success0,success1})));
        res.body() = "{\"success\": " + min({success0,success1}) + ",\"reason\":\"group not updated\"}";
    }else if(type=="edit_group"){
        sql_args = {name,id};
        success = executeSQL(type, sql_args);
        sql_args.clear();
        if(success=="1"){
            sql_args.push_back(id);
            success0 = executeSQL("delete_group_users", sql_args);
            sql_args.clear();
            vector <string> members_vec = splitString(members, ' ');
            for(vector<string>::iterator i = members_vec.begin(); i!=members_vec.end(); i++){
                sql_args={id,*i};
                success1 = executeSQL("add_user_group",sql_args);
                sql_args.clear();
            }
            sql_args.push_back(id);
            success2 = executeSQL("delete_group_permissions", sql_args);
            sql_args.clear();
            vector <string> readables = splitString(readable, ' ');
            for(vector<string>::iterator i = readables.begin(); i!=readables.end(); i++){
                sql_args={id,*i};
                success3 = executeSQL("add_group_permission",sql_args);
                sql_args.clear();
            }
        }
        res.result(set_status(min({success,success0,success1,success2,success3})));
        res.body() = "{\"success\": " + min({success,success0,success1,success2,success3}) + ",\"reason\":\"group not edited\"}";
    }else if(type=="set_pass"){
        sql_args = {password,password,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\"user not updated\"}");
    }else if(type=="edit_license"){
        sql_args = {name,rate,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\"qualification not updated\"}");
    }else if(type=="add_license"){
        sql_args = {name,rate};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\"license not added\"}");
    }else if(type=="edit_slot"){
        sql_args = {name,begin,end,duration,rate,active,id};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\"slot not updated\"}");
    }else if(type=="add_slot"){
        sql_args = {name,begin,end,duration,rate,active};
        success = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success+",\"reason\":\"slot not added\"}");
    }else if(type=="get_pupils_empty"){
        sql_args = {sessionid,list_end,list_begin,slot_id};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_dates_list"){
        sql_args = {slot_id,list_end,list_begin};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_competitors_list"){
        year_end = year;
        year.append("-01-01");
        year_end.append("-12-31");
        sql_args.push_back(year_end);
        sql_args.push_back(year);
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_salary_data_trainings"){
        sql_args={sessionid,month,month};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_salary_data_exams" || type=="get_salary_data_competitions"){
        sql_args={sessionid,month,month,id};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="get_judoka_trainings"){
        sql_args = {list_end,list_begin,slot_id,sessionid};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="delete_slot"){
        sql_args.push_back(id);
        result = executeSQL("get_slot_training", sql_args);
        if(result != ""){
            success = "0";
            reason = "slot not empty";
        }else{
            success = executeSQL(type,sql_args);
            reason = "slot not deleted";
        }
        sql_args.clear();
        res.result(set_status(success));
        res.body() =("{\"success\":"+ success +",\"reason\":\""+reason+"\"}");
    }else if(type=="delete_license"){
        sql_args.push_back(id);
        success0 = executeSQL(type,sql_args);
        success1 = executeSQL("delete_license_judoka",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1})));
        res.body() =("{\"success\":"+ min({success0,success1})+",\"reason\":\"qualification not deleted\"}");
    }else if(type=="delete_group"){
        sql_args.push_back(id);
        success0 = executeSQL(type,sql_args);
        success1 = executeSQL("delete_group_users",sql_args);
        success2 = executeSQL("delete_group_permissions",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1,success2})));
        res.body() =("{\"success\":"+ min({success0,success1,success2})+",\"reason\":\"group not deleted\"}");
    }else if(type=="show_judoka_delete"){
        sql_args = {date,date,date,date,date,date};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="delete_empty_entries"){
        sql_args={};
        success0 = executeSQL("delete_empty_training",sql_args);
        success1 = executeSQL("delete_empty_comp",sql_args);
        success2 = executeSQL("delete_empty_exam",sql_args);
        sql_args.clear();
        res.result(set_status(min({success0,success1,success2})));
        res.body() =("{\"success\":"+ min({success0,success1,success2})+",\"reason\":\"empty entries partially not deleted\"}");
    }else if(type=="get_average_slot" || type=="get_count_licenses" || type=="get_exam_grades"|| type=="get_competition_results"){
        sql_args = {};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="show_user_info"){
        sql_args = {sessionid};
        result = executeSQL(type,sql_args);
        sql_args.clear();
        res.result(set_status(result));
        res.body() = result;
    }else if(type=="dummy"){
        res.result(boost::beast::http::status::ok);
        res.body() = ("[{\"success\":1,\"reason\":\"Dummy Call\"}]");
    }else{
        res.result(boost::beast::http::status::internal_server_error);
        res.body() = ("{\"success\":0,\"reason\":\"unknown  API call\"}");
    }
return res;
}
