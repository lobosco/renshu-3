#include "common.h"

using namespace std;

void init_logging()
{
    std::cout << "initialize logging" << std::endl;
    
    logging::add_common_attributes();
    
    logging::core::get()->set_filter
    (
        logging::trivial::severity >= logging::trivial::info
    );
    
    auto timestamp = expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S.%f");
    auto severity = expr::attr<logging::trivial::severity_level>("Severity");
    
    logging::formatter logFormat = expr::format("[%1%] [%2%] %3%") % timestamp % severity % expr::smessage; 
    
    auto filelog = logging::add_file_log(
             keywords::file_name = "/var/log/renshu/renshu_%Y-%m-%d_%N.log",                                          
             keywords::rotation_size = 5 * 1024 * 1024,                                     
             keywords::time_based_rotation = sinks::file::rotation_at_time_point(12, 0, 0)
        );
             
    filelog->set_formatter(logFormat);
    filelog->locked_backend()->auto_flush(true);
    
    auto console_log = logging::add_console_log(std::clog);
    console_log->set_formatter(logFormat);
}

int main(int argc, char *argv[]){
    
    Utility u = Utility();
    
    
    bool database = false;
    bool server = false;
    bool help = false;
    std::string dbName="";
    std::string dbAdress="";
    std::string dbUser= "";
    std::string dbPass= "";
    std::string serverAdr = "";
    std::string serverPort = "";
    std::string serverDocRoot = "";
    std::string configfile = "";
    
    init_logging();
    
    //read command line input
    for (int i = 0; i < argc; i++) {
        if (std::string(argv[i]) == "-server" || std::string(argv[i]) == "-srv") {
            server = true;
            serverAdr = argv[i+1];
            serverPort = argv[i+2];
            serverDocRoot = argv[i+3];
        }
        if(std::string(argv[i]) == "-database" || std::string(argv[i]) == "-db") {
            database = true;
            dbName = argv[i + 1];
            dbAdress = argv[i + 2];
            dbUser = argv[i + 3];
            dbPass = argv[i + 4];
        }
        if(std::string(argv[i]) == "-configfile" || std::string(argv[i]) == "-conf") {
            database = true;
            configfile = argv[i + 1];
            BOOST_LOG_TRIVIAL(info) << "configuration file: "<<configfile;
            int linenr = 0;
            std::string line;
            ifstream myfile (configfile);
            if (myfile.is_open()){
                while ( getline (myfile,line) ){
                    switch(linenr){
                        case 0:
                            serverAdr=line;
                            linenr++;
                            break;
                        case 1:
                            serverPort=line;
                            linenr++;
                            break;
                        case 2:
                            serverDocRoot=line;
                            linenr++;
                            break;
                        case 3:
                            dbName=line;
                            linenr++;
                            break;
                        case 4:
                            dbAdress=line;
                            linenr++;
                            break;
                        case 5:
                            dbUser=line;
                            linenr++;
                            break;
                        case 6:
                            dbPass=line;
                            linenr++;
                            break;
                    }
                }
            myfile.close();
            BOOST_LOG_TRIVIAL(info)<<"configuration file read"<<endl;
            server = true;
            database = true;
            }else{
                BOOST_LOG_TRIVIAL(error)<< "Unable to open configuration file";
                return 0;
            }
        }
        if(std::string(argv[i]) == "-help" || std::string(argv[i]) == "-h") {
            cout<<"Renshu 3.0 Backend:\n\n Usage:\n=====\n\n -server,-srv \"<adr>,<port>,<docroot>\" \t starts server listening to adress <adr> on port <port> with document root (needed for tls certificate files) <docroot>\n-database,db \"<name>,<adr>,<user>,<pass>\" \t Database details: Name,Address,Username,Password\n-configfile,conf <file> \t loads configuration file specified in <file>\n -help,-h\t displays this help\n\n";
            help = true;
        }
    }
    u.initDB(dbAdress,dbName,dbUser,dbPass);
    
    if(server){
        BOOST_LOG_TRIVIAL(info)<<"server: "<<serverAdr << " " << serverPort << " " << serverDocRoot << "\n";
        start_server(serverAdr,serverPort, serverDocRoot, "1", serverDocRoot+"./fullchain.pem", serverDocRoot+"./privkey.pem", serverDocRoot+"./dh.pem", &u);
    }
    
    if(help){
        BOOST_LOG_TRIVIAL(info) << "help displayed";
    }
    
    BOOST_LOG_TRIVIAL(info)<<"end of processing" <<endl;

    return EXIT_SUCCESS;
}



