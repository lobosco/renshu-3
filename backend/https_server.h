#ifndef _HTTPSERVER_H_
#define _HTTPSERVER_H_

// global Header
#include "common.h"

void start_server(std::string address, std::string port, std::string doc_root, std::string threads, std::string certificate_path, std::string private_key_path, std::string dh_path, Utility* u);
#endif